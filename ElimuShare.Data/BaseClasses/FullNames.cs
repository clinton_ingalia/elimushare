﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.BaseClasses
{
    public class FullNames
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
    }
}
