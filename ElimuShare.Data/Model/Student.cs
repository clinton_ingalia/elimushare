﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.Model
{
    public class Student
    {
        [Key]
        public Guid ID { get; set; }
        public string AdmissionNumber { get; set; }
        public Guid SchoolId { get; set; }
        public Guid ParentId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        public string ImgUrl { get; set; }
        public string ClassLevel { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
