﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model.Shared
{
    public class SchoolType
    {
        public string Category { get; set; }
        public bool Boarding { get; set; }
        public string Gender { get; set; }
    }
}
