﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model.Shared
{
    public class County
    {
        public string Name { get; set; }
        public int Code { get; set; }
    }
}
