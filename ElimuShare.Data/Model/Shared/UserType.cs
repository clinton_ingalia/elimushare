﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model.Shared
{
    public class UserType
    {
        [Key]
        public Guid ID { get; set; }
        public string Category { get; set; }
        public int AccessLevel { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
