﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model
{
    public class PaymentMethod
    {
        [Key]
        public Guid ID { get; set; }
        public Guid SchoolId { get; set; }
        public string Name { get; set; }
        public string AccountName { get; set; }
        public int PayBill { get; set; }
        public int TillNumber { get; set; }
        public int AccountNumber { get; set; }
        public string Instruction { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
