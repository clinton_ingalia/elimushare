﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model
{
    public class Notification
    {
        [Key]
        public Guid ID { get; set; }
        public Guid SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Public { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
