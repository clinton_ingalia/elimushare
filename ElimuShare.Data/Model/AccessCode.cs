﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model
{
    public class AccessCode
    {
        public Guid ID { get; set; }
        public Guid ParentId { get; set; }
        public int Code { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
