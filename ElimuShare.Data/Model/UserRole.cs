﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model
{
    public class UserAccessRole
    {
        [Key]
        public Guid ID { get; set; }    
        public string Role { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
