﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.Model
{
    public class Parent
    {
        [Key]
        public Guid ID { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        [Required]
        public string MobileNumber { get; set; }
        [Required]
        public byte[] Password { get; set; }
        public string PIN { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public Location Location { get; set; }
        public County County { get; set; }
        public string ImgUrl { get; set; }
        public bool Activated { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
