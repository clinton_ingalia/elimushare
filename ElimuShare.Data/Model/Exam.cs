﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.Model
{
    public class Exam
    {
        [Key]
        public Guid ID { get; set; }
        public Guid SchoolId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Term { get; set; }
        public DateTime Year { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
