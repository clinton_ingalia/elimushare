﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.Model
{
    public class User
    {
        [Key]
        public Guid ID { get; set; }
        public Guid UserRoleId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        [Required]
        public string MobileNumber { get; set; }
        [Required]
        public byte[] Password { get; set; }
        public string ImgUrl { get; set; }
        public Location Location { get; set; }
        public County County { get; set; }
        public string Gender { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
