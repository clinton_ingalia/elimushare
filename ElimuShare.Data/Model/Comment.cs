﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model
{
    public class Comment
    {
        public Guid ID { get; set; }
        public Guid UserOrParentId { get; set; }
        public Guid EventOrNotificationId { get; set; }
        public string Description { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
