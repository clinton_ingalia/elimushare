﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model
{
    public class ExamScore
    {
        [Key]
        public Guid ID { get; set; }
        public Guid StudentId { get; set; }
        public Guid ExamId { get; set; }
        public Guid SchoolId { get; set; }
        public string Title { get; set; }
        public DateTime Year { get; set; }
        public int Term { get; set; }
        public int Position { get; set; }
        public int TotalStudents { get; set; }
        public int Score { get; set; }
        public int TotalScore { get; set; }
        public string Grade { get; set; }
        public string TeacherRemarks { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
