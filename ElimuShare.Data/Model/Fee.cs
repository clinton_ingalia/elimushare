﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Model
{
    public class Fee
    {
        [Key]
        public Guid ID { get; set; }
        public Guid StudentId { get; set; }
        public Guid SchoolId { get; set; }
        public Guid PaymentMethodId { get; set; }
        public DateTime Year { get; set; }
        public int Term { get; set; }
        public double Amount { get; set; }
        public double Paid { get; set; }
        public double Balance { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
