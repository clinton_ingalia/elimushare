﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.Model
{
    public class School
    {
        [Key]
        public Guid ID { get; set; }
        public Guid AdministratorId { get; set; }
        public string Name { get; set; }
        public string LogoUrl { get; set; }
        public string ImgUrl { get; set; }
        public Location Location { get; set; }
        public County County { get; set; }
        public SchoolType SchoolType { get; set; }
        public string Bio { get; set; }
        public string Mission { get; set; }
        public string Vission { get; set; }
        public string Motto { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string AddressLine { get; set; }
        public int PostalCode { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
