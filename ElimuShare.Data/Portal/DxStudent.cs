﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Portal
{
    public class DxStudent
    {
        public Guid ID { get; set; }
        public Guid SchoolId { get; set; }
        public string Parent { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        public string ImgUrl { get; set; }
        public string ClassLevel { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
