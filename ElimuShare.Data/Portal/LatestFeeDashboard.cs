﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Portal
{
    public class LatestFeeDashboard
    {
        public Guid ID { get; set; }
        public string StudentName { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime Year { get; set; }
        public int Term { get; set; }
        public double Amount { get; set; }
        public double Paid { get; set; }
        public double Balance { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
