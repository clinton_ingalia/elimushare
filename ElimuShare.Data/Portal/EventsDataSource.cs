﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Portal
{
    public class EventsDataSource
    {
        public string text { get; set; }
        public List<int> ownerId { get; set; }
       public DateTime startDate { get; set; }
       public DateTime endDate { get; set; }
    }
}
