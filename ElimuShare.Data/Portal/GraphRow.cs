﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Portal
{
    public class GraphRow
    {
        public int TotalExams { get; set; }
        public int TotalStudents { get; set; }
        public List<GradeSummary> GradeSummary { get; set; }
        public List<TopSubjects> TopSubjects { get; set; }
        public string BestSubject { get; set; }
        public string WorstSubject { get; set; }
        public int AllTimeBestScore { get; set; }
        public List<FeesData> FeesData { get; set; }
        public double TotalPaid { get; set; }
        public double TotalUnpaid { get; set; }
    }

    public class GradeSummary
    {
        public string Grade { get; set; }
        public int Total { get; set; }
    }

    public class TopSubjects
    {
        public string Term { get; set; }
        public int TopSubject { get; set; }
        public int SecondHighestSubject { get; set; }
        public int ThirdHighestSubject { get; set; }
    }

    public class FeesData
    {
        public string Month { get; set; }
        public double Paid { get; set; }
        public double Unpaid { get; set; }
    }
}
