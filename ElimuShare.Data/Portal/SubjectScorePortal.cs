﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Portal
{
    public class SubjectScorePortal
    {
        public Guid ID { get; set; }
        public string Subject { get; set; }
        public string Exam { get; set; }
        public string Student { get; set; }
        public string Title { get; set; }
        public DateTime Year { get; set; }
        public int Term { get; set; }
        public int Score { get; set; }
        public int TotalScore { get; set; }
        public string Grade { get; set; }
        public string TeacherRemarks { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
