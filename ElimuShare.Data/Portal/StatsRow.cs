﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.Portal
{
    public class StatsRow
    {
        public int Students { get; set; }
        public int Parents { get; set; }
        public int Events { get; set; }
        public int Vacancies { get; set; }
    }
}
