﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel.Common
{
    public class ResultVM
    {
        public Guid StudentId { get; set; }
        public Guid SchoolId { get; set; }
        public string Title { get; set; }
    }
}
