﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.ViewModel.Common
{
    public class SubjectVM
    {
        public Guid SchoolId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
