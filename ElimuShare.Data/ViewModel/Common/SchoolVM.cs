﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.ViewModel.Common
{
    public class SchoolVM
    {
        public Guid AdministratorId { get; set; }
        public string Name { get; set; }
        public string LogoUrl { get; set; }
        public string SchoolCategory { get; set; }
        public bool Boarding { get; set; }
        public string SchoolGender { get; set; }
        public string Bio { get; set; }
        public string Mission { get; set; }
        public string Vission { get; set; }
        public string Motto { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

    }
}
