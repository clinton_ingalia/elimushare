﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel.Common
{
    public class UserTypeVM
    {
        public string Category { get; set; }
        public int AccessLevel { get; set; }
    }
}
