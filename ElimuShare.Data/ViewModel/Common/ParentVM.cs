﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.ViewModel
{
    public class ParentVM
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        public string MobileNumber { get; set; }
        public string Password { get; set; }
        public string PIN { get; set; }
        public string Email { get; set; }
        public Location Location { get; set; }
        public County County { get; set; }
        public string ImgUrl { get; set; }
        public string Gender { get; set; }
    }

    public class ParentEditSchoolVM
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        public string MobileNumber { get; set; }
        public string Gender { get; set; }
    }
}
