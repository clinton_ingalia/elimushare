﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel.Common
{
    public class PaymentMethodVM
    {
        public Guid SchoolId { get; set; }
        public string Name { get; set; }
        public string AccountName { get; set; }
        public int PayBill { get; set; }
        public int TillNumber { get; set; }
        public int AccountNumber { get; set; }
        public string Instruction { get; set; }
    }
}
