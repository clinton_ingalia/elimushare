﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.ViewModel.Common
{
    public class VacancyVM
    {
        public Guid SchoolId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Number { get; set; }
    }

    public class VacancyDisplay
    {
        public Guid ID { get; set; }
        public Guid SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Number { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
