﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel.Common
{
    public class CommentVM
    {
        public Guid UserOrParentId { get; set; }
        public Guid EventOrNotificationId { get; set; }
        public string Description { get; set; }
    }
}
