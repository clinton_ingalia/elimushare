﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel.Common
{
    public class NotificationVM
    {
        public Guid SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Public { get; set; }
    }
}
