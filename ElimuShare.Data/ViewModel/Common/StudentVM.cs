﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.ViewModel.Common
{
    public class StudentVM
    {
        public Guid SchoolId { get; set; }
        public Guid ParentId { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        public string ImgUrl { get; set; }
        public string ClassLevel { get; set; }
    }
}
