﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel.Common
{
    public class ParentStudentVM
    {
        public Guid SchoolId { get; set; }
        public string ParentFirstName { get; set; }
        public string ParentSecondName { get; set; }
        public string ParentSurName { get; set; }
        public string ParentMobileNumber { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        public string ImgUrl { get; set; }
        public string ClassLevel { get; set; }
    }
}
