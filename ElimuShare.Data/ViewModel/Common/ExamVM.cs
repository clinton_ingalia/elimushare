﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElimuShare.Data.Model.Shared;

namespace ElimuShare.Data.ViewModel.Common
{
    public class ExamVM
    {
        public Guid SchoolId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Term { get; set; }
        public DateTime Year { get; set; }
    }
}
