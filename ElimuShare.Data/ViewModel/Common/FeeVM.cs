﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel.Common
{
    public class FeeVM
    {
        public Guid StudentId { get; set; }
        public Guid SchoolId { get; set; }
        public Guid PaymentMethodId { get; set; }
        public DateTime Year { get; set; }
        public double Paid { get; set; }
        public int Term { get; set; }
        public double Amount { get; set; }
        public double Balance { get; set; }
    }

    public class FeeUploaderVM
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SurName { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime Year { get; set; }
        public double Paid { get; set; }
        public int Term { get; set; }
        public double Amount { get; set; }
        public double Balance { get; set; }
    }
}
