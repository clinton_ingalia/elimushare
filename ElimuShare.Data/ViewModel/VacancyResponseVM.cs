﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel
{
    public class VacancyResponseVM
    {
        public Guid VacancyId { get; set; }
        public string ParentNames { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string StudentNames { get; set; }
        public string PreviousSchoolName { get; set; }
        public string Qualifications { get; set; }
        public string ChildDescription { get; set; }
    }
}
