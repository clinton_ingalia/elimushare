﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Data.ViewModel
{
    public class Login
    {
        public string MobileNumber { get; set; }
        public string Password { get; set; }
    }
}
