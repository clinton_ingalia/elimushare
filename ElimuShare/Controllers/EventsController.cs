﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Model.Shared;
using ElimuShare.Data.Portal;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class EventsController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        [Authorize]
        // GET: api/Events
        public IQueryable<Event> GetEvents()
        {
            return db.Events;
        }
        [Authorize]
        // GET: api/EventsBySchoolId
        [HttpGet]
        [Route("api/EventsBySchoolId")]
        public IQueryable<Event> EventsBySchoolId(Guid SchoolId)
        {
            return db.Events.Where(x => x.SchoolId == SchoolId);
        }
        [Authorize]
        // GET: api/LatestEventsBySchoolId
        [HttpGet]
        [Route("api/LatestEventsBySchoolId")]
        public IQueryable<Event> LatestEventsBySchoolId(Guid SchoolId)
        {
            return db.Events.Where(x => x.SchoolId == SchoolId).OrderByDescending(x => x.Timestamp).Take(4);
        }
        [Authorize]
        // GET: api/DTventsBySchoolId
        [HttpGet]
        [Route("api/DTventsBySchoolId")]
        public List<EventsDataSource> DTventsBySchoolId(Guid SchoolId)
        {
            var data2 = db.Events.ToList();
            var datas = db.Events.Where(x => x.SchoolId == SchoolId).ToList();
            var eventsList = new List<EventsDataSource>();
            Random random = new Random();
            var owner = new List<int>();
            foreach (var data in datas)
            {
                owner.Add(random.Next(0, 4));
                EventsDataSource eventsDataSource = new EventsDataSource()
                {
                    text = data.Title,
                    endDate = data.EndDate,
                    startDate = data.StartDate,
                    ownerId = owner
                };
                eventsList.Add(eventsDataSource);
            }
           
            return eventsList;
        }
        [Authorize]
        // GET: api/Events/5
        [ResponseType(typeof(Event))]
        public async Task<IHttpActionResult> GetEvent(Guid id)
        {
            Event @event = await db.Events.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            return Ok(@event);
        }
        [Authorize]
        // PUT: api/Events/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEvent(Guid id, EventVM @eventVM)
        {
            Event @event = new Event()
            {
                ID = id,
                Title = @eventVM.Title,
                Location = new Location()
                {
                    Name = @eventVM.LocationName,
                    Longitude = @eventVM.Longitude,
                    Latitude = @eventVM.Latitude
                },
                ownerId = @eventVM.ownerId,
                StartDate = @eventVM.StartDate,
                EndDate = @eventVM.EndDate,
                Description = @eventVM.Description,
                SchoolId = @eventVM.SchoolId,
                Time = @eventVM.Time,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != @event.ID)
            {
                return BadRequest();
            }

            db.Entry(@event).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/MultipleEvents
        [HttpPost]
        [Route("api/MultipleEvents")]
        [ResponseType(typeof(List<Event>))]
        public HttpResponseMessage MultipleEvents(List<EventVMMultiple> eventsVM, Guid schoolid)
        {
            try
            {
                foreach (var eventVM in eventsVM)
                {
                    Event @event = new Event()
                    {
                        ID = Guid.NewGuid(),
                        Title = @eventVM.Title,
                        Location = new Location()
                        {
                            Name = @eventVM.LocationName,
                            Longitude = 1,
                            Latitude = 2
                        },
                        ownerId = new List<int>() {1,2,3},
                        StartDate = @eventVM.StartDate,
                        EndDate = @eventVM.EndDate,
                        Description = @eventVM.Description,
                        SchoolId = schoolid,
                        Time = @eventVM.Time,
                        Timestamp = DateTime.Now
                    };
                    db.Events.Add(@event);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, eventsVM);
        }
        [Authorize]
        // POST: api/Events
        [ResponseType(typeof(Event))]
        public async Task<IHttpActionResult> PostEvent(EventVM @eventVM)
        {
            Event @event = new Event()
            {
                ID = Guid.NewGuid(),
                Title = @eventVM.Title,
                Location = new Location()
                {
                    Name = @eventVM.LocationName,
                    Longitude = @eventVM.Longitude,
                    Latitude = @eventVM.Latitude
                },
                ownerId = @eventVM.ownerId,
                StartDate = @eventVM.StartDate,
                EndDate = @eventVM.EndDate,
                Description = @eventVM.Description,
                SchoolId = @eventVM.SchoolId,
                Time = @eventVM.Time,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Events.Add(@event);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EventExists(@event.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = @event.ID }, @event);
        }
        [Authorize]
        // DELETE: api/Events/5
        [ResponseType(typeof(Event))]
        public async Task<IHttpActionResult> DeleteEvent(Guid id)
        {
            Event @event = await db.Events.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            db.Events.Remove(@event);
            await db.SaveChangesAsync();

            return Ok(@event);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool EventExists(Guid id)
        {
            return db.Events.Count(e => e.ID == id) > 0;
        }
    }
}