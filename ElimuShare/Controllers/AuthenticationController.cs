﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using ElimuShare.Data.Model;
using ElimuShare.Data.ViewModel;
using ElimuShare.Models;
using ElimuShare.Services.HashService;
using ElimuShare.Services.SMSService;

namespace ElimuShare.Controllers
{
    public class AuthenticationController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        private HashService _hashService = new HashService();
        private AuthenticationRepository _authenticationRepository = new AuthenticationRepository();

        [HttpPost]
        [Route("api/Login")]
        public HttpResponseMessage Login(Login login)
        {
            return _authenticationRepository.UniversalLogin(login);
        }

        [HttpPost]
        [Route("api/ParentLogin")]
        public HttpResponseMessage ParentLogin(Login login)
        {
            return _authenticationRepository.Login(1, login.MobileNumber, login.Password);
        }

        [HttpPost]
        [Route("api/ParentForgotPassword")]
        public HttpResponseMessage ParentForgotPassword(ForgotPassword forgotPassword)
        {
            var user = db.Parents.Where(u => u.MobileNumber == forgotPassword.MobileNumber)
                .FirstOrDefault();
            if (user == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No user found with that Mobile Number");
            }
            else
            {
                string generatedPassword = Membership.GeneratePassword(8, 4);
                byte[] hashed = _hashService.ComputeHashByte(user.ID.ToString().Replace(@"-", ""), generatedPassword);
                user.Password = hashed;
                try
                {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    SMS sms = new SMS();
                    sms.SendSMS(user.MobileNumber, "Hello " + user.FirstName + ", Your new password is " + generatedPassword + ", kindly login and update your password");
                    return Request.CreateResponse(HttpStatusCode.OK, user);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return Request.CreateResponse(HttpStatusCode.NotModified, e.Message);
                }
            }
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("api/UserForgotPassword")]
        public HttpResponseMessage UserForgotPassword(ForgotPassword forgotPassword)
        {
            var user = db.Users.Where(u => u.MobileNumber == forgotPassword.MobileNumber)
                .FirstOrDefault();
            if (user == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No user found with that Mobile Number");
            }
            else
            {
                string generatedPassword = Membership.GeneratePassword(8, 4);
                byte[] hashed = _hashService.ComputeHashByte(user.ID.ToString().Replace(@"-", ""), generatedPassword);
                user.Password = hashed;
                try
                {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    SMS sms = new SMS();
                    sms.SendSMS(user.MobileNumber, "Hello " + user.FirstName + ", Your new password is " + generatedPassword + ", kindly login and update your password");
                    return Request.CreateResponse(HttpStatusCode.OK, user);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return Request.CreateResponse(HttpStatusCode.NotModified, e.Message);
                }
            }
        }

        [HttpPost]
        [Route("api/UserLogin")]
        public HttpResponseMessage UserLogin(Login login)
        {
            return _authenticationRepository.Login(0, login.MobileNumber, login.Password);
        }

        [Authorize]
        [HttpGet]
        [Route("api/AccessCode")]
        public HttpResponseMessage AccessCode(Guid ParentId)
        {
            var accessCode = db.AccessCodes.Where(u => u.ParentId == ParentId)
                .FirstOrDefault();
            var parent = db.Parents.Where(x => x.ID == ParentId).FirstOrDefault();
            //if no code create one
            if (accessCode == null && parent != null)
            {
                Random rnd = new Random();
                AccessCode newAccessCode = new AccessCode()
                {
                    ID = Guid.NewGuid(),
                    ParentId = ParentId,
                    Code = rnd.Next(1000, 10000),
                    Timestamp = DateTime.Now
                };
                //send sms
                db.AccessCodes.Add(newAccessCode);
                db.SaveChanges();
                SMS sms = new SMS();
                sms.SendSMS(parent.MobileNumber, "Hello " + parent.FirstName + ", Your Code is " + newAccessCode.Code + ". Code expires after 180 days");
                return Request.CreateResponse(HttpStatusCode.Created, newAccessCode);
            }
            else
            {
                //check if code has expired after 48 hours
                TimeSpan diff = DateTime.Now - accessCode.Timestamp;
                if (diff.Days > 180)
                {
                    //code expired create new and delete old one
                    db.AccessCodes.Remove(accessCode);
                    db.SaveChanges();
                    Random rnd = new Random();
                    AccessCode newAccessCode = new AccessCode()
                    {
                        ID = Guid.NewGuid(),
                        ParentId = ParentId,
                        Code = rnd.Next(1000, 10000),
                        Timestamp = DateTime.Now
                    };
                    //send sms
                    SMS sms = new SMS();
                    sms.SendSMS(parent.MobileNumber, "Hello " + parent.FirstName + ", Your Code is " + newAccessCode.Code + ". Code expires after 180 days");
                    return Request.CreateResponse(HttpStatusCode.Created, newAccessCode);
                }
                else
                {
                    //send code
                    SMS sms = new SMS();
                    sms.SendSMS(parent.MobileNumber, "Hello " + parent.FirstName + ", Your Code is " + accessCode.Code + ". Code expires after 180 days");
                    return Request.CreateResponse(HttpStatusCode.Created, accessCode);
                }

            }


        }
    }
}
