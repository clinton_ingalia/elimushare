﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;
using Remotion.Data.Linq.Clauses;

namespace ElimuShare.Controllers
{
    public class VacanciesController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        [Authorize]
        // GET: api/Vacancies
        public HttpResponseMessage GetVacancies()
        {
           
            var vacs = db.Vacancies.ToList();
            var schs = db.Schools.ToList();
            var vacList = new List<VacancyDisplay>();
            foreach (var vac in vacs)
            {
                var school = schs.Where(x => x.ID == vac.SchoolId).FirstOrDefault();
                if (school == null) continue;
                var newVac = new VacancyDisplay()
                {
                    ID = vac.ID,
                    SchoolId = vac.SchoolId,
                    Title = vac.Title,
                    Description = vac.Description,
                    SchoolName = school.Name,
                    Number = vac.Number,
                    Timestamp = vac.Timestamp
                };
                vacList.Add(newVac);
            }
                       
            
            return Request.CreateResponse(HttpStatusCode.OK, vacList);
        }
        [Authorize]
        // GET: api/VacanciesBySchoolId
        [HttpGet]
        [Route("api/VacanciesBySchoolId")]
        public IQueryable<Vacancy> VacanciesBySchoolId(Guid SchoolId)
        {
            return db.Vacancies.Where(x => x.SchoolId == SchoolId);
        }
        [Authorize]
        // GET: api/Vacancies/5
        [ResponseType(typeof(Vacancy))]
        public async Task<IHttpActionResult> GetVacancy(Guid id)
        {
            Vacancy vacancy = await db.Vacancies.FindAsync(id);
            if (vacancy == null)
            {
                return NotFound();
            }

            return Ok(vacancy);
        }
        [Authorize]
        // PUT: api/Vacancies/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVacancy(Guid id, VacancyVM vacancyVM)
        {
            Vacancy vacancy = new Vacancy()
            {
                ID = id,
                Description = vacancyVM.Description,
                Number = vacancyVM.Number,
                Name = vacancyVM.Name,
                SchoolId = vacancyVM.SchoolId,
                Title = vacancyVM.Title,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vacancy.ID)
            {
                return BadRequest();
            }

            db.Entry(vacancy).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VacancyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/MultipleVacancies
        [HttpPost]
        [Route("api/MultipleVacancies")]
        [ResponseType(typeof(List<Vacancy>))]
        public HttpResponseMessage MultipleVacancies(List<VacancyVM> vacanciesVM)
        {


            try
            {
                foreach (var vacancyVM in vacanciesVM)
                {
                    Vacancy vacancy = new Vacancy()
                    {
                        ID = Guid.NewGuid(),
                        Description = vacancyVM.Description,
                        Number = vacancyVM.Number,
                        Name = vacancyVM.Name,
                        SchoolId = vacancyVM.SchoolId,
                        Title = vacancyVM.Title,
                        Timestamp = DateTime.Now
                    };
                    db.Vacancies.Add(vacancy);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, vacanciesVM);
        }
        [Authorize]
        // POST: api/Vacancies
        [ResponseType(typeof(Vacancy))]
        public async Task<IHttpActionResult> PostVacancy(VacancyVM vacancyVM)
        {
            Vacancy vacancy = new Vacancy()
            {
                ID = Guid.NewGuid(),
                Description = vacancyVM.Description,
                Number = vacancyVM.Number,
                Name = vacancyVM.Name,
                SchoolId = vacancyVM.SchoolId,
                Title = vacancyVM.Title,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Vacancies.Add(vacancy);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VacancyExists(vacancy.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = vacancy.ID }, vacancy);
        }
        [Authorize]
        // DELETE: api/Vacancies/5
        [ResponseType(typeof(Vacancy))]
        public async Task<IHttpActionResult> DeleteVacancy(Guid id)
        {
            Vacancy vacancy = await db.Vacancies.FindAsync(id);
            if (vacancy == null)
            {
                return NotFound();
            }

            db.Vacancies.Remove(vacancy);
            await db.SaveChangesAsync();

            return Ok(vacancy);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool VacancyExists(Guid id)
        {
            return db.Vacancies.Count(e => e.ID == id) > 0;
        }
    }
}