﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ElimuShare.Services.EmailService;
using ElimuShare.Services.SMSService;

namespace ElimuShare.Controllers
{
    public class EmailController : ApiController
    {
        [Authorize]
        [HttpGet]
        [Route("api/SingleEmail")]
        public void SingleEmail(string emailAddress, string message)
        {
            Email email = new Email();
            email.sendEmail(emailAddress, message);
        }
    }
}
