﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Models;
using ElimuShare.Data.ViewModel.Common;

namespace ElimuShare.Controllers
{
    public class NotificationsController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        [Authorize]
        // GET: api/Notifications
        public IQueryable<Notification> GetNotifications()
        {
            return db.Notifications.Take(20).OrderByDescending(x => x.Timestamp);
        }
        [Authorize]
        // GET: api/Notifications/5
        [ResponseType(typeof(Notification))]
        public async Task<IHttpActionResult> GetNotification(Guid id)
        {
            Notification notification = await db.Notifications.FindAsync(id);
            if (notification == null)
            {
                return NotFound();
            }

            return Ok(notification);
        }
        [Authorize]
        // GET: api/NotificationsBySchoolId
        [HttpGet]
        [Route("api/NotificationsBySchoolId")]
        public IQueryable<Notification> NotificationsBySchoolId(Guid SchoolId)
        {
            return db.Notifications.Where(x => x.SchoolId == SchoolId);
        }
        [Authorize]
        // PUT: api/Notifications/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutNotification(Guid id, NotificationVM notificationVM)
        {
            Notification notification = new Notification()
            {
                ID = id,
                Title = notificationVM.Title,
                Description = notificationVM.Description,
                SchoolId = notificationVM.SchoolId,
                SchoolName = notificationVM.SchoolName,
                Public = notificationVM.Public,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != notification.ID)
            {
                return BadRequest();
            }

            db.Entry(notification).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NotificationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/MultipleNotifications
        [HttpPost]
        [Route("api/MultipleNotifications")]
        [ResponseType(typeof(List<Notification>))]
        public HttpResponseMessage MultipleNotifications(List<NotificationVM> notificationsVM)
        {


            try
            {
                foreach (var notificationVM in notificationsVM)
                {
                    Notification notification = new Notification()
                    {
                        ID = Guid.NewGuid(),
                        Title = notificationVM.Title,
                        Description = notificationVM.Description,
                        SchoolId = notificationVM.SchoolId,
                        SchoolName = notificationVM.SchoolName,
                        Public = notificationVM.Public,
                        Timestamp = DateTime.Now
                    };
                    db.Notifications.Add(notification);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, notificationsVM);
        }

        // POST: api/Notifications
        [Authorize]
        [ResponseType(typeof(Notification))]
        public async Task<IHttpActionResult> PostNotification(NotificationVM notificationVM)
        {
            Notification notification = new Notification()
            {
                ID = Guid.NewGuid(),
                Title = notificationVM.Title,
                Description = notificationVM.Description,
                SchoolId = notificationVM.SchoolId,
                SchoolName = notificationVM.SchoolName,
                Public = notificationVM.Public,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Notifications.Add(notification);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (NotificationExists(notification.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = notification.ID }, notification);
        }

        // DELETE: api/Notifications/5
        [Authorize]
        [ResponseType(typeof(Notification))]
        public async Task<IHttpActionResult> DeleteNotification(Guid id)
        {
            Notification notification = await db.Notifications.FindAsync(id);
            if (notification == null)
            {
                return NotFound();
            }

            db.Notifications.Remove(notification);
            await db.SaveChangesAsync();

            return Ok(notification);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool NotificationExists(Guid id)
        {
            return db.Notifications.Count(e => e.ID == id) > 0;
        }
    }
}