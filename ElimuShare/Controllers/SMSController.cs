﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ElimuShare.Services.SMSService;

namespace ElimuShare.Controllers
{
    public class SMSController : ApiController
    {
        [Authorize]
        [HttpGet]
        [Route("api/SingleSMS")]
        public void SingleSMS(string phone, string data)
        {
            SMS sms = new SMS();
            sms.SendSMS(phone, data);
        }
    }
}
