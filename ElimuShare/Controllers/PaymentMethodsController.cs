﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class PaymentMethodsController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        [Authorize]
        // GET: api/PaymentMethods
        public IQueryable<PaymentMethod> GetPaymentMethods()
        {
            return db.PaymentMethods;
        }
        [Authorize]
        // GET: api/PaymentMethods/5
        [ResponseType(typeof(PaymentMethod))]
        public async Task<IHttpActionResult> GetPaymentMethod(Guid id)
        {
            PaymentMethod paymentMethod = await db.PaymentMethods.FindAsync(id);
            if (paymentMethod == null)
            {
                return NotFound();
            }

            return Ok(paymentMethod);
        }
        [Authorize]
        // GET: api/PaymentMethodsBySchoolId/5
        [HttpGet]
        [Route("api/PaymentMethodsBySchoolId")]
        [ResponseType(typeof(PaymentMethod))]
        public HttpResponseMessage PaymentMethodsBySchoolId(Guid SchoolId)
        {
            var data = db.PaymentMethods.Where(x => x.SchoolId == SchoolId).ToList();
            return data.Count == 0 ? Request.CreateResponse(HttpStatusCode.NotFound, "No payment methods found") : Request.CreateResponse(HttpStatusCode.OK, data);
        }
        [Authorize]
        // PUT: api/PaymentMethods/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPaymentMethod(Guid id, PaymentMethodVM paymentMethodVM)
        {
            var paymentMethod = new PaymentMethod()
            {
                ID = id,
                AccountName = paymentMethodVM.AccountName,
                AccountNumber = paymentMethodVM.AccountNumber,
                Instruction = paymentMethodVM.Instruction,
                Name = paymentMethodVM.Name,
                PayBill = paymentMethodVM.PayBill,
                SchoolId = paymentMethodVM.SchoolId,
                TillNumber = paymentMethodVM.TillNumber,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != paymentMethod.ID)
            {
                return BadRequest();
            }

            db.Entry(paymentMethod).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentMethodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/PaymentMethods
        [ResponseType(typeof(PaymentMethod))]
        public async Task<IHttpActionResult> PostPaymentMethod(PaymentMethodVM paymentMethodVM)
        {
            var paymentMethod = new PaymentMethod()
            {
                ID = Guid.NewGuid(),
                AccountName = paymentMethodVM.AccountName,
                AccountNumber = paymentMethodVM.AccountNumber,
                Instruction = paymentMethodVM.Instruction,
                Name = paymentMethodVM.Name,
                PayBill = paymentMethodVM.PayBill,
                SchoolId = paymentMethodVM.SchoolId,
                TillNumber = paymentMethodVM.TillNumber,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PaymentMethods.Add(paymentMethod);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PaymentMethodExists(paymentMethod.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = paymentMethod.ID }, paymentMethod);
        }
        [Authorize]
        // DELETE: api/PaymentMethods/5
        [ResponseType(typeof(PaymentMethod))]
        public async Task<IHttpActionResult> DeletePaymentMethod(Guid id)
        {
            PaymentMethod paymentMethod = await db.PaymentMethods.FindAsync(id);
            if (paymentMethod == null)
            {
                return NotFound();
            }

            db.PaymentMethods.Remove(paymentMethod);
            await db.SaveChangesAsync();

            return Ok(paymentMethod);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool PaymentMethodExists(Guid id)
        {
            return db.PaymentMethods.Count(e => e.ID == id) > 0;
        }
    }
}