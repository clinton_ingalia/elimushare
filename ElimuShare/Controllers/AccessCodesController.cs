﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class AccessCodesController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();

        // GET: api/AccessCodes
        [Authorize]
        public IQueryable<AccessCode> GetAccessCodes()
        {
            return db.AccessCodes;
        }
        [Authorize]
        // GET: api/AccessCodes/5
        [ResponseType(typeof(AccessCode))]
        public async Task<IHttpActionResult> GetAccessCode(Guid id)
        {
            AccessCode accessCode = await db.AccessCodes.FindAsync(id);
            if (accessCode == null)
            {
                return NotFound();
            }

            return Ok(accessCode);
        }
        [Authorize]
        // PUT: api/AccessCodes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAccessCode(Guid id, AccessCode accessCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accessCode.ID)
            {
                return BadRequest();
            }

            db.Entry(accessCode).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccessCodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/AccessCodes
        [ResponseType(typeof(AccessCode))]
        public async Task<IHttpActionResult> PostAccessCode(AccessCode accessCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AccessCodes.Add(accessCode);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AccessCodeExists(accessCode.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = accessCode.ID }, accessCode);
        }
        [Authorize]
        // DELETE: api/AccessCodes/5
        [ResponseType(typeof(AccessCode))]
        public async Task<IHttpActionResult> DeleteAccessCode(Guid id)
        {
            AccessCode accessCode = await db.AccessCodes.FindAsync(id);
            if (accessCode == null)
            {
                return NotFound();
            }

            db.AccessCodes.Remove(accessCode);
            await db.SaveChangesAsync();

            return Ok(accessCode);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool AccessCodeExists(Guid id)
        {
            return db.AccessCodes.Count(e => e.ID == id) > 0;
        }
    }
}