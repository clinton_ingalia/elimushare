﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using ElimuShare.Data.Model;
using ElimuShare.Models;
using LinqToExcel;
using inficore.DocumentProcessor;

namespace ElimuShare.Controllers
{
    [RoutePrefix("api/Upload")]
    public class UploadController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        
        [Route("SchoolGenerateExcel")]
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage SchoolGenerateExcel(Guid SchoolId)
        {
            //ToDo: Incomplete SchoolGenerateExcel
            var currentDate = DateTime.Now;
            if (SchoolId == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Empty models");
            var school = db.Schools.Find(SchoolId);
            try
            {
                if (school != null)
                {

                    string[] subjectArray = new string[20];
                    var subjects = db.Subjects.Where(x => x.SchoolId == SchoolId).ToList();
                    subjectArray[1] = "FirstName";
                    subjectArray[2] = "SecondName";
                    subjectArray[3] = "SurName";
                    subjectArray[4] = "Exam";
                    subjectArray[5] = "Year";
                    subjectArray[6] = "Term";
                    subjectArray[7] = "Score";
                    subjectArray[7] = "TotalScore";
                    subjectArray[7] = "Position";
                    subjectArray[7] = "TotalStudents";
                    subjectArray[7] = "Grade";
                    subjectArray[8] = "TeacherRemarks";
                    var i = 9;
                    foreach (var subject in subjects)
                    {
                        subjectArray[i] = subject.Name;
                        i++;
                    }


                    string sPath = "";
                    string Path = "~/Document/" + "Exam Results Uploader" + "-" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + "-";
                    sPath = HttpContext.Current.Server.MapPath(Path);
                    //create directory if it does not exist.
                    bool exists = Directory.Exists(sPath);
                    if (!exists)
                    {
                        Directory.CreateDirectory(sPath);
                    }

                    ExcelGenerator excelGenerator = new ExcelGenerator();
                    var docUrl = excelGenerator.GenerateWorkbook(subjectArray, school.Name, sPath);
                    return Request.CreateResponse(HttpStatusCode.OK, docUrl);

                }
                return Request.CreateResponse(HttpStatusCode.NotFound, "School not found");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
            

        }
        [Authorize]
        [Route("School/SchoolLogo")]
        [AllowAnonymous]
        [HttpPost]
        public string SchoolLogo()
        {
            int iUploadedCnt = 0;
            
            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string resultData = "";
            string sPath = "";
            sPath = HttpContext.Current.Server.MapPath("~/Images/SchoolLogos/");
            //create directory if it does not exist.
            bool exists = Directory.Exists(sPath);
            if (!exists)
            {
                Directory.CreateDirectory(sPath);
            }            
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            try
            {
                // CHECK THE FILE COUNT.
                for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
                {
                    System.Web.HttpPostedFile hpf = hfc[iCnt];

                    if (hpf.ContentLength > 0)
                    {
                        // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                        if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                        {
                            // SAVE THE FILES IN THE FOLDER.
                            // check if file exist.
                            bool fExists = File.Exists(sPath);
                            if (!fExists)
                            {
                                hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                                iUploadedCnt = iUploadedCnt + 1;
                                resultData = BaseSiteUrl + "/Images/SchoolLogos/" + Path.GetFileName(hpf.FileName);
                            }
                            else
                            {
                                resultData = "File already exist";
                            }
                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            
            // RETURN A MESSAGE (OPTIONAL).
            return iUploadedCnt > 0 ? resultData : "Upload Failed";
        }




        // get a sites base urll ex: example.com
        public static string BaseSiteUrl
        {
            get
            {
                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
                return baseUrl;
            }

        }

        [Route("School/Students")]
        [Authorize]
        [HttpPost]
        public HttpResponseMessage Students()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string resultData = "";
            string sPath = "";

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            try
            {
                // CHECK THE FILE COUNT.
                for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
                {
                    System.Web.HttpPostedFile hpf = hfc[iCnt];

                    if (hpf.ContentLength > 0)
                    {
                        iUploadedCnt = iUploadedCnt + 1;
                        var excel = new ExcelQueryFactory(hpf.FileName);
                        var students = from s in excel.Worksheet<Student>()
                                   select s;
                        string[] headerRow = excel.GetColumnNames("Sheet1").ToArray();
                        return Request.CreateResponse(HttpStatusCode.OK, students);
                        //foreach (var student in students)
                        //{
                        //    //return student;
                        //    //db.Students.Add(student);
                        //}
                        //db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e);
            }
            return Request.CreateResponse(HttpStatusCode.ExpectationFailed, hfc);
        }
    }
}
