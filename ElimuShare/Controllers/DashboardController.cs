﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ElimuShare.Data.Model;
using ElimuShare.Data.Portal;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class DashboardController : ApiController
    {
        public readonly ElimuShareContext db = new ElimuShareContext();


        [Authorize]
        [HttpGet]
        [Route("api/StatsRow")]
        public HttpResponseMessage StatsRow(Guid SchoolId)
        {
            int parents = GetParentsBySchoolId(SchoolId);
            int students = db.Students.Count(x => x.SchoolId == SchoolId);
            int events = db.Events.Count(x => x.SchoolId == SchoolId);
            int vacancies = db.Vacancies.Count(x => x.SchoolId == SchoolId);
            var statsRow = new StatsRow()
            {
                Parents = parents,
                Students = students,
                Events = events,
                Vacancies = vacancies
            };

            return Request.CreateResponse(HttpStatusCode.OK, statsRow);
        }
        [Authorize]
        [HttpGet]
        [Route("api/GraphRow")]
        public HttpResponseMessage GraphRow(Guid SchoolId)
        {
            var currentDate = new DateTime();
            int exams = db.ExamScores.Count(x => x.SchoolId == SchoolId);
            int students = db.Students.Count(x => x.SchoolId == SchoolId);
            var subjectScores = db.SubjectScores.Where(x => x.SchoolId == SchoolId);
            var fees = db.Fees.Where(x => x.SchoolId == SchoolId).ToList();
            int allA = 0;
            int allB = 0;
            int allC = 0;
            int allD = 0;
            int allE = 0;
            var topSubjectsList = new List<TopSubjects>();

            var lastSixMonths = Enumerable
                .Range(0, 5)
                .Select(i => DateTime.Now.AddMonths(i - 5));
            double paid = 0, unpaid = 0;
            foreach (var fee in fees)
            {
                paid = paid + fee.Paid;
                unpaid = unpaid + (fee.Amount - fee.Paid);
            }

            var feesDataList = new List<FeesData>();
            foreach (var lastSixMonth in lastSixMonths)
            {
                var feeDatas = fees.Where(x => x.Timestamp.Month == lastSixMonth.Month).ToList();
                string m = "";
                double p = 0, u = 0;

                foreach (var feeData in feeDatas)
                {
                    m = feeData.Timestamp.ToString("MMMM");
                    p = p + feeData.Paid;
                    u = u + feeData.Amount - feeData.Paid;
                }
                FeesData feesData = new FeesData()
                {
                    Month = m,
                    Paid = p,
                    Unpaid = u
                };
                feesDataList.Add(feesData);
            }

            foreach (var subjectScore in subjectScores)
            {

                if (subjectScore.Grade == "A" || subjectScore.Grade == "A-")
                {
                    allA++;
                }
                else if (subjectScore.Grade == "B" || subjectScore.Grade == "B-" || subjectScore.Grade == "B+")
                {
                    allB++;
                }
                else if (subjectScore.Grade == "C" || subjectScore.Grade == "C-" || subjectScore.Grade == "C+")
                {
                    allC++;
                }
                else if (subjectScore.Grade == "D" || subjectScore.Grade == "D-" || subjectScore.Grade == "D+")
                {
                    allD++;
                }
                else if (subjectScore.Grade == "E")
                {
                    allE++;
                }
            }
            var term1HighestScores = subjectScores.Where(x => x.Term == 1).OrderByDescending(x => x.Score).Take(5).ToList();
            if (term1HighestScores.Count != 0)
            {
                int count = term1HighestScores.Count;
                switch (count)
                {
                    case 1:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 1", TopSubject = term1HighestScores.ElementAt(0).Score, SecondHighestSubject = 0, ThirdHighestSubject = 0 });
                        break;
                    case 2:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 1", TopSubject = term1HighestScores.ElementAt(0).Score, SecondHighestSubject = term1HighestScores.ElementAt(1).Score, ThirdHighestSubject = 0 });
                        break;
                    default:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 1", TopSubject = term1HighestScores.ElementAt(0).Score, SecondHighestSubject = term1HighestScores.ElementAt(1).Score, ThirdHighestSubject = term1HighestScores.ElementAt(2).Score });
                        break;
                }
            }
            


            var term2HighestScores = subjectScores.Where(x => x.Term == 2).OrderByDescending(x => x.Score).Take(3).ToList();
            if (term2HighestScores.Count != 0)
            {
                int count = term2HighestScores.Count;
                switch (count)
                {
                    case 1:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 2", TopSubject = term2HighestScores.ElementAt(0).Score, SecondHighestSubject = 0, ThirdHighestSubject = 0 });
                        break;
                    case 2:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 2", TopSubject = term2HighestScores.ElementAt(0).Score, SecondHighestSubject = term2HighestScores.ElementAt(1).Score, ThirdHighestSubject = 0 });
                        break;
                    default:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 2", TopSubject = term2HighestScores.ElementAt(0).Score, SecondHighestSubject = term2HighestScores.ElementAt(1).Score, ThirdHighestSubject = term2HighestScores.ElementAt(2).Score });
                        break;
                }
            }




            var term3HighestScores = subjectScores.Where(x => x.Term == 3).OrderByDescending(x => x.Score).Take(3).ToList();
            if (term3HighestScores.Count != 0)
            {
                int count = term3HighestScores.Count;
                switch (count)
                {
                    case 1:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 3", TopSubject = term3HighestScores.ElementAt(0).Score, SecondHighestSubject = 0, ThirdHighestSubject = 0 });
                        break;
                    case 2:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 3", TopSubject = term3HighestScores.ElementAt(0).Score, SecondHighestSubject = term3HighestScores.ElementAt(1).Score, ThirdHighestSubject = 0 });
                        break;
                    default:
                        topSubjectsList.Add(new TopSubjects() { Term = "Term 3", TopSubject = term3HighestScores.ElementAt(0).Score, SecondHighestSubject = term3HighestScores.ElementAt(1).Score, ThirdHighestSubject = term3HighestScores.ElementAt(2).Score });
                        break;
                }
            }


            var HighestScores = subjectScores.OrderByDescending(x => x.Score).FirstOrDefault();
            var LowestScores = subjectScores.OrderBy(x => x.Score).FirstOrDefault();


            var gradesSummaryList = new List<GradeSummary>();
            gradesSummaryList.Add(new GradeSummary() { Grade = "A", Total = allA });
            gradesSummaryList.Add(new GradeSummary() { Grade = "B", Total = allB });
            gradesSummaryList.Add(new GradeSummary() { Grade = "C", Total = allC });
            gradesSummaryList.Add(new GradeSummary() { Grade = "D", Total = allD });
            gradesSummaryList.Add(new GradeSummary() { Grade = "E", Total = allE });
            var graphRow = new GraphRow()
            {
                TotalStudents = students,
                TotalExams = exams,
                GradeSummary = gradesSummaryList,
                TopSubjects = topSubjectsList,
                AllTimeBestScore = HighestScores.Score,
                BestSubject = HighestScores.Title,
                WorstSubject = LowestScores.Title,
                FeesData = feesDataList,
                TotalPaid = paid,
                TotalUnpaid = unpaid
            };

            return Request.CreateResponse(HttpStatusCode.OK, graphRow);
        }
        [Authorize]
        [HttpGet]
        [Route("api/SchoolStatsGraph")]
        public HttpResponseMessage SchoolStatsGraph(Guid SchoolId)
        {
            var schoolStatsGraphList = new List<SchoolStatsGraph>();
            var lastSixMonths = Enumerable
                .Range(0, 5)
                .Select(i => DateTime.Now.AddMonths(i - 5));

            foreach (var month in lastSixMonths)
            {
                int parents = GetParentsBySchoolId(SchoolId, month);
                int students = db.Students.Count(x => x.SchoolId == SchoolId && x.Timestamp.Month == month.Month);
                int events = db.Events.Count(x => x.SchoolId == SchoolId && x.Timestamp.Month == month.Month);
                int vacancies = db.Vacancies.Count(x => x.SchoolId == SchoolId && x.Timestamp.Month == month.Month);
                var schoolStatsGraph = new SchoolStatsGraph()
                {
                    Month = month,
                    MonthName = month.ToString("MMMM"),
                    Parents = parents,
                    Students = students,
                    Events = events,
                    Vacancies = vacancies
                };
                schoolStatsGraphList.Add(schoolStatsGraph);
            }
            //get for current month
            int parentsCM = GetParentsBySchoolId(SchoolId, DateTime.Now);
            int studentsCM = db.Students.Count(x => x.SchoolId == SchoolId && x.Timestamp.Month == DateTime.Now.Month);
            int eventsCM = db.Events.Count(x => x.SchoolId == SchoolId && x.Timestamp.Month == DateTime.Now.Month);
            int vacanciesCM = db.Vacancies.Count(x => x.SchoolId == SchoolId && x.Timestamp.Month == DateTime.Now.Month);
            var schoolStatsGraphCM = new SchoolStatsGraph()
            {
                Month = DateTime.Now,
                MonthName = DateTime.Now.ToString("MMMM"),
                Parents = parentsCM,
                Students = studentsCM,
                Events = eventsCM,
                Vacancies = vacanciesCM
            };
            schoolStatsGraphList.Add(schoolStatsGraphCM);
            return Request.CreateResponse(HttpStatusCode.OK, schoolStatsGraphList.OrderBy(x => x.Month));
        }

        [Authorize]
        public int GetParentsBySchoolId(Guid SchoolId)
        {
            var students = db.Students.Where(x => x.SchoolId == SchoolId).ToList();
            var parents = db.Parents.ToList();
            var parentsList = new List<Parent>();
            foreach (var student in students)
            {
                foreach (var parent in parents.ToList())
                {
                    if (student.ParentId == parent.ID)
                    {
                        parentsList.Add(parent);
                    }
                }
            }
            var listWithNoDuplicates = parentsList.Distinct().ToList();
            return listWithNoDuplicates.Count;
        }
        [Authorize]
        public int GetParentsBySchoolId(Guid SchoolId, DateTime month)
        {
            var students = db.Students.Where(x => x.SchoolId == SchoolId).ToList();
            var parents = db.Parents.Where(x => x.Timestamp.Month == month.Month).ToList();
            var parentsList = new List<Parent>();
            foreach (var student in students)
            {
                foreach (var parent in parents.ToList())
                {
                    if (student.ParentId == parent.ID)
                    {
                        parentsList.Add(parent);
                    }
                }
            }
            var listWithNoDuplicates = parentsList.Distinct().ToList();
            return listWithNoDuplicates.Count;
        }
    }
}
