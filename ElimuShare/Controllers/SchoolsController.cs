﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Model.Shared;
using ElimuShare.Data.ViewModel;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class SchoolsController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        [Authorize]
        // GET: api/Schools
        public IQueryable<School> GetSchools()
        {
            return db.Schools;
        }

        [Authorize]
        [HttpGet]
        [Route("api/SchoolByFilter")]
        public IQueryable<School> SchoolByFilter(string County, bool Boarding, string Gender, string Category)
        {
            return db.Schools.Where(x => x.County.Name == County && x.SchoolType.Boarding == Boarding &&
                                                x.SchoolType.Category == Category && x.SchoolType.Gender == Gender);
        }
        [Authorize]
        [HttpGet]
        [Route("api/SchoolByAdministratorId")]
        public School SchoolByAdministratorId(Guid AdministratorId)
        {
            return db.Schools.Where(x => x.AdministratorId == AdministratorId).FirstOrDefault();
        }
        [Authorize]
        // GET: api/Schools/5
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> GetSchool(Guid id)
        {
            School school = await db.Schools.FindAsync(id);
            if (school == null)
            {
                return NotFound();
            }

            return Ok(school);
        }


        [Authorize]
        // PUT: api/Schools/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSchool(Guid id, School school)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != school.ID)
            {
                return BadRequest();
            }

            db.Entry(school).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SchoolExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/Schools
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> PostSchool(SchoolVM schoolVM)
        {
            School school = new School()
            {
                ID = Guid.NewGuid(),
                LogoUrl = schoolVM.LogoUrl,
                SchoolType = new SchoolType()
                {
                    Gender = schoolVM.SchoolGender,
                    Boarding = schoolVM.Boarding,
                    Category = schoolVM.SchoolCategory
                },
                AdministratorId = schoolVM.AdministratorId,
                Bio = schoolVM.Bio,
                Name = schoolVM.Name,
                Email = schoolVM.Email,
                Mission = schoolVM.Mission,
                Motto = schoolVM.Motto,
                Phone = schoolVM.Phone,
                Vission = schoolVM.Vission,
                AddressLine = "Not set",
                County = new County()
                {
                    Name = "Not set",
                    Code = 0
                },
                ImgUrl = "Not set",
                Location = new Location()
                {
                    Name = "Not set",
                    Longitude = 1,
                    Latitude = 2
                },
                PostalCode = 0000,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Schools.Add(school);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SchoolExists(school.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = school.ID }, school);
        }
        [Authorize]
        // DELETE: api/Schools/5
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> DeleteSchool(Guid id)
        {
            School school = await db.Schools.FindAsync(id);
            if (school == null)
            {
                return NotFound();
            }

            db.Schools.Remove(school);
            await db.SaveChangesAsync();

            return Ok(school);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool SchoolExists(Guid id)
        {
            return db.Schools.Count(e => e.ID == id) > 0;
        }
    }
}