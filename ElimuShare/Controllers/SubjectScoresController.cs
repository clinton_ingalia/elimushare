﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Portal;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class SubjectScoresController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        [Authorize]
        // GET: api/SubjectScores
        public IQueryable<SubjectScore> GetSubjectScores()
        {
            return db.SubjectScores;
        }
        [Authorize]
        // GET: api/SubjectScores/5
        [ResponseType(typeof(SubjectScore))]
        public async Task<IHttpActionResult> GetSubjectScore(Guid id)
        {
            SubjectScore subjectScore = await db.SubjectScores.FindAsync(id);
            if (subjectScore == null)
            {
                return NotFound();
            }

            return Ok(subjectScore);
        }
        [Authorize]
        // GET: api/SubjectScoreByExamId
        [HttpGet]
        [Route("api/SubjectScoreByExamId")]
        public IQueryable<SubjectScore> SubjectScoreByExamId(Guid ExamId)
        {
            return db.SubjectScores.Where(x => x.ExamId == ExamId);
        }
        [Authorize]
        // GET: api/SubjectScoreBySchoolId
        [HttpGet]
        [Route("api/SubjectScoreBySchoolId")]
        public IQueryable<SubjectScore> SubjectScoreBySchoolId(Guid SchoolId)
        {
            return db.SubjectScores.Where(x => x.SchoolId == SchoolId);
        }
        [Authorize]
        // GET: api/DxSubjectScoreBySchoolId
        [HttpGet]
        [Route("api/DxSubjectScoreBySchoolId")]
        public IQueryable<SubjectScorePortal> DxSubjectScoreBySchoolId(Guid SchoolId)
        {
            var query = (from subjectScore in db.SubjectScores
                         join exam in db.Exams on subjectScore.ExamId equals exam.ID
                         join student in db.Students on subjectScore.StudentId equals student.ID
                         join subject in db.Subjects on subjectScore.SubjectId equals subject.ID
                         where subjectScore.SchoolId == SchoolId
                         select new SubjectScorePortal()
                         {
                             
                             Timestamp = subjectScore.Timestamp,
                             ID = subjectScore.ID,
                             Score = subjectScore.Score,
                             Title = subjectScore.Title,
                             Term = subjectScore.Term,
                             Year = subjectScore.Year,
                             Student = student.FirstName + " " + student.SurName,
                             Subject = subject.Name,
                             Exam = exam.Name,
                             TotalScore = subjectScore.TotalScore,
                             Grade = subjectScore.Grade,
                             TeacherRemarks = subjectScore.TeacherRemarks
                         }
                );



            return query;
        }


        [Authorize]
        // GET: api/SubjectScoreByStudentId
        [HttpGet]
        [Route("api/SubjectScoreByStudentId")]
        public IQueryable<SubjectScore> SubjectScoreByStudentId(Guid StudentId)
        {
            return db.SubjectScores.Where(x => x.StudentId == StudentId);
        }
        [Authorize]
        // PUT: api/SubjectScores/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSubjectScore(Guid id, SubjectScoreVM subjectScoreVM)
        {
            var subjectScore = new SubjectScore()
            {
                ID = id,
                SchoolId = subjectScoreVM.SchoolId,
                Score = subjectScoreVM.Score,
                StudentId = subjectScoreVM.StudentId,
                SubjectId = subjectScoreVM.SubjectId,
                TeacherRemarks = subjectScoreVM.TeacherRemarks,
                Term = subjectScoreVM.Term,
                Title = subjectScoreVM.Title,
                TotalScore = subjectScoreVM.TotalScore,
                Year = subjectScoreVM.Year,
                ExamId = subjectScoreVM.ExamId,
                Grade = subjectScoreVM.Grade,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subjectScore.ID)
            {
                return BadRequest();
            }

            db.Entry(subjectScore).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubjectScoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/MultipleSubjectScores
        [HttpPost]
        [Route("api/MultipleSubjectScores")]
        [ResponseType(typeof(List<SubjectScore>))]
        public HttpResponseMessage MultipleSubjectScores(List<StudentSubjectVM> studentsSubjectVM, Guid schoolId)
        {


            try
            {
                foreach (var studentSubjectVM in studentsSubjectVM)
                {
                    var student = db.Students.Where(x => x.FirstName == studentSubjectVM.FirstName &&
                                                         x.SecondName == studentSubjectVM.SecondName &&
                                                         x.SurName == studentSubjectVM.SurName)
                        .FirstOrDefault();
                    var exam = db.Exams.Where(x => x.Name == studentSubjectVM.Exam).FirstOrDefault();
                    var subject = db.Subjects.Where(x => x.Name == studentSubjectVM.Subject).FirstOrDefault();

                    if (student == null || exam == null || subject == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotAcceptable,
                            "Exam or Student or Subject have not been created. Please create the students or exams before uploading their data.");
                    }

                    SubjectScore subjectScore = new SubjectScore()
                    {
                        ID = Guid.NewGuid(),
                        SchoolId = schoolId,
                        Score = studentSubjectVM.Score,
                        StudentId = student.ID,
                        SubjectId = subject.ID,
                        TeacherRemarks = studentSubjectVM.TeacherRemarks,
                        Term = studentSubjectVM.Term,
                        Title = studentSubjectVM.Title,
                        TotalScore = studentSubjectVM.TotalScore,
                        Year = studentSubjectVM.Year,
                        ExamId = exam.ID,
                        Grade = studentSubjectVM.Grade,
                        Timestamp = DateTime.Now
                    };
                    db.SubjectScores.Add(subjectScore);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, studentsSubjectVM);
        }
        [Authorize]
        // POST: api/SubjectScores
        [ResponseType(typeof(SubjectScore))]
        public async Task<IHttpActionResult> PostSubjectScore(SubjectScoreVM subjectScoreVM)
        {
            var subjectScore = new SubjectScore()
            {
                ID = Guid.NewGuid(),
                SchoolId = subjectScoreVM.SchoolId,
                Score = subjectScoreVM.Score,
                StudentId = subjectScoreVM.StudentId,
                SubjectId = subjectScoreVM.SubjectId,
                TeacherRemarks = subjectScoreVM.TeacherRemarks,
                Term = subjectScoreVM.Term,
                Title = subjectScoreVM.Title,
                TotalScore = subjectScoreVM.TotalScore,
                Year = subjectScoreVM.Year,
                ExamId = subjectScoreVM.ExamId,
                Grade = subjectScoreVM.Grade,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SubjectScores.Add(subjectScore);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SubjectScoreExists(subjectScore.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = subjectScore.ID }, subjectScore);
        }
        [Authorize]
        // DELETE: api/SubjectScores/5
        [ResponseType(typeof(SubjectScore))]
        public async Task<IHttpActionResult> DeleteSubjectScore(Guid id)
        {
            SubjectScore subjectScore = await db.SubjectScores.FindAsync(id);
            if (subjectScore == null)
            {
                return NotFound();
            }

            db.SubjectScores.Remove(subjectScore);
            await db.SaveChangesAsync();

            return Ok(subjectScore);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool SubjectScoreExists(Guid id)
        {
            return db.SubjectScores.Count(e => e.ID == id) > 0;
        }
    }
}