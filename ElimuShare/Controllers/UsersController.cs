﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Model.Shared;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;
using ElimuShare.Services.HashService;
using ElimuShare.Services.SMSService;

namespace ElimuShare.Controllers
{
    public class UsersController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        private readonly UserRepository _repo = new UserRepository();
        private HashService _hashService = new HashService();
        [Authorize]
        // GET: api/Users
        public IQueryable<User> GetUsers()
        {
            return db.Users;
        }
        [Authorize]
        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetUser(Guid id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }
        [Authorize]
        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(Guid id, UserVM userVM)
        {
            Guid newId = id;
            byte[] hashed = _hashService.ComputeHashByte(newId.ToString().Replace(@"-", ""), userVM.Password);
            User user = new User()
            {
                ID = newId,
                UserRoleId = userVM.UserRoleId,
                FirstName = userVM.FirstName,
                SecondName = userVM.SecondName,
                SurName = userVM.SurName,
                ImgUrl = userVM.ImgUrl,
                Gender = userVM.Gender,
                MobileNumber = userVM.MobileNumber,
                Password = hashed,
                County = userVM.County,
                Location = userVM.Location,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.ID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        //[Authorize]
        // POST: api/Users
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostUser(UserRegisterVM userVM)
        {
            Guid newId = Guid.NewGuid();
            byte[] hashed = _hashService.ComputeHashByte(newId.ToString().Replace(@"-", ""), userVM.Password);
            User user = new User()
            {
                ID = newId,
                UserRoleId = userVM.UserRoleId,
                FirstName = userVM.FirstName,
                SecondName = userVM.SecondName,
                SurName = userVM.SurName,
                MobileNumber = userVM.MobileNumber,
                Password = hashed,
                Gender = userVM.Gender,
                County = new County()
                {
                    Name = "Not Set",
                    Code = 00
                },
                ImgUrl = "NoImageSet",
                Location = new Location()
                {
                    Name = "NotSet",
                    Latitude = 2,
                    Longitude = 1
                },
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_repo.IsRegistered(user.MobileNumber))
            {
                return Conflict();
            }
            db.Users.Add(user);

            try
            {
                await db.SaveChangesAsync();
                SMS sms = new SMS();
                sms.SendSMS(userVM.MobileNumber, "Hello " + userVM.FirstName + ", Welcome to Somanet, you have successfully created an account");
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.ID))
                {
                    return Conflict();
                }              
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = user.ID }, user);
        }
        [Authorize]
        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser(Guid id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool UserExists(Guid id)
        {
            return db.Users.Count(e => e.ID == id) > 0;
        }
    }
}