﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.ViewModel;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class VacancyResponsesController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        [Authorize]
        // GET: api/VacancyResponses
        public IQueryable<VacancyResponse> GetVacancyResponses()
        {
            return db.VacancyResponses;
        }
        [Authorize]
        // GET: api/VacancyResponses/5
        [ResponseType(typeof(VacancyResponse))]
        public async Task<IHttpActionResult> GetVacancyResponse(Guid id)
        {
            VacancyResponse vacancyResponse = await db.VacancyResponses.FindAsync(id);
            if (vacancyResponse == null)
            {
                return NotFound();
            }

            return Ok(vacancyResponse);
        }
        [Authorize]
        // PUT: api/VacancyResponses/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVacancyResponse(Guid id, VacancyResponseVM vacancyResponseVM)
        {
            var vacancyResponse = new VacancyResponse()
            {
                ID = id,
                ChildDescription = vacancyResponseVM.ChildDescription,
                Email = vacancyResponseVM.Email,
                ParentNames = vacancyResponseVM.ParentNames,
                Phone = vacancyResponseVM.Phone,
                PreviousSchoolName = vacancyResponseVM.PreviousSchoolName,
                Qualifications = vacancyResponseVM.Qualifications,
                StudentNames = vacancyResponseVM.StudentNames,
                VacancyId = vacancyResponseVM.VacancyId,
                Timestamp = DateTime.Now

            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vacancyResponse.ID)
            {
                return BadRequest();
            }

            db.Entry(vacancyResponse).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VacancyResponseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/VacancyResponses
        [ResponseType(typeof(VacancyResponse))]
        public async Task<IHttpActionResult> PostVacancyResponse(VacancyResponseVM vacancyResponseVM)
        {
            var vacancyResponse = new VacancyResponse()
            {
                ID = Guid.NewGuid(),
                ChildDescription = vacancyResponseVM.ChildDescription,
                Email = vacancyResponseVM.Email,
                ParentNames = vacancyResponseVM.ParentNames,
                Phone = vacancyResponseVM.Phone,
                PreviousSchoolName = vacancyResponseVM.PreviousSchoolName,
                Qualifications = vacancyResponseVM.Qualifications,
                StudentNames = vacancyResponseVM.StudentNames,
                VacancyId = vacancyResponseVM.VacancyId,
                Timestamp = DateTime.Now
                
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VacancyResponses.Add(vacancyResponse);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VacancyResponseExists(vacancyResponse.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = vacancyResponse.ID }, vacancyResponse);
        }
        [Authorize]
        // DELETE: api/VacancyResponses/5
        [ResponseType(typeof(VacancyResponse))]
        public async Task<IHttpActionResult> DeleteVacancyResponse(Guid id)
        {
            VacancyResponse vacancyResponse = await db.VacancyResponses.FindAsync(id);
            if (vacancyResponse == null)
            {
                return NotFound();
            }

            db.VacancyResponses.Remove(vacancyResponse);
            await db.SaveChangesAsync();

            return Ok(vacancyResponse);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool VacancyResponseExists(Guid id)
        {
            return db.VacancyResponses.Count(e => e.ID == id) > 0;
        }
    }
}