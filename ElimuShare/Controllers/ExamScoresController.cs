﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Portal;
using ElimuShare.Models;
using ElimuShare.Data.ViewModel;
using ElimuShare.Data.ViewModel.Common;

namespace ElimuShare.Controllers
{
    public class ExamScoresController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        private StudentsController studentsController = new StudentsController();

        // GET: api/ExamScores
        [Authorize]
        public IQueryable<ExamScore> GetExamScores()
        {
            return db.ExamScores;
        }

        // GET: api/ExamScores/5
        [Authorize]
        [ResponseType(typeof(ExamScore))]
        public async Task<IHttpActionResult> GetExamScore(Guid id)
        {
            ExamScore examScore = await db.ExamScores.FindAsync(id);
            if (examScore == null)
            {
                return NotFound();
            }

            return Ok(examScore);
        }


        // GET: api/DxExamScoreBySchoolId
        [Authorize]
        [HttpGet]
        [Route("api/DxExamScoreBySchoolId")]
        public IQueryable<ExamScorePortal> DxExamScoreBySchoolId(Guid SchoolId)
        {
            var query = (from examScore in db.ExamScores
                join exam in db.Exams on examScore.ExamId equals exam.ID
                join student in db.Students on examScore.StudentId equals student.ID
                where examScore.SchoolId == SchoolId
                select new ExamScorePortal()
                {

                    Timestamp = examScore.Timestamp,
                    ID = examScore.ID,
                    Score = examScore.Score,
                    Title = examScore.Title,
                    Term = examScore.Term,
                    Year = examScore.Year,
                    Student = student.FirstName + " " + student.SurName,
                    Exam = exam.Name,
                    TotalScore = examScore.TotalScore,
                    Grade = examScore.Grade,
                    TeacherRemarks = examScore.TeacherRemarks,
                    TotalStudents = examScore.TotalStudents,
                    Position = examScore.Position
                }
            );



            return query;
        }

        // GET: api/ExamScoreByStudentId
        [Authorize]
        [HttpGet]
        [Route("api/ExamScoreByStudentId")]
        public List<ExamScoreResults> ExamScoreByStudentId(Guid StudentId)
        {
            var examScores = db.ExamScores.Where(x => x.StudentId == StudentId).ToList();
            var subjectScoresList = new List<SubjectScore>();
            var examScoresResultsList = new List<ExamScoreResults>();
            var subjectScores = db.SubjectScores.Where(x => x.StudentId == StudentId).ToList();
            foreach (var examScore in examScores.ToList())
            {
                foreach (var subjectScore in subjectScores.ToList())
                {
                    if (examScore.ExamId == subjectScore.ExamId && examScore.Year.Year == subjectScore.Year.Year && examScore.Term == subjectScore.Term && examScore.StudentId == subjectScore.StudentId)
                    {
                        subjectScoresList.Add(subjectScore);
                    }

                }
                var examScoreResults = new ExamScoreResults()
                {
                    ID = examScore.ID,
                    SchoolId = examScore.SchoolId,
                    ExamId = examScore.ExamId,
                    Grade = examScore.Grade,
                    Position = examScore.Position,
                    Score = examScore.Score,
                    StudentId = examScore.StudentId,
                    SubjectScores = subjectScoresList,
                    TeacherRemarks = examScore.TeacherRemarks,
                    Term = examScore.Term,
                    Title = examScore.Title,
                    TotalScore = examScore.TotalScore,
                    TotalStudents = examScore.TotalStudents,
                    Year = examScore.Year,
                    Timestamp = examScore.Timestamp
                };
                examScoresResultsList.Add(examScoreResults);
            }
            return examScoresResultsList;
        }

        // GET: api/ExamScoreBySchoolId
        [Authorize]
        [HttpGet]
        [Route("api/ExamScoreBySchoolId")]
        public IQueryable<ExamScore> ExamScoreBySchoolId(Guid SchoolId)
        {
            return db.ExamScores.Where(x => x.SchoolId == SchoolId);
        }

        // PUT: api/ExamScores/5
        [Authorize]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutExamScore(Guid id, ExamScoreVM examScoreVM)
        {
            ExamScore examScore = new ExamScore()
            {
                ID = id,
                ExamId = examScoreVM.ExamId,
                Grade = examScoreVM.Grade,
                Position = examScoreVM.Position,
                SchoolId = examScoreVM.SchoolId,
                Score = examScoreVM.Score,
                StudentId = examScoreVM.StudentId,
                TeacherRemarks = examScoreVM.TeacherRemarks,
                Term = examScoreVM.Term,
                Title = examScoreVM.Title,
                TotalScore = examScoreVM.TotalScore,
                TotalStudents = examScoreVM.TotalStudents,
                Year = examScoreVM.Year,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != examScore.ID)
            {
                return BadRequest();
            }

            db.Entry(examScore).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExamScoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MultipleExamScores
        [Authorize]
        [HttpPost]
        [Route("api/MultipleExamScores")]
        [ResponseType(typeof(List<ExamScore>))]
        public HttpResponseMessage MultipleExamScores(List<StudentExamVM> studentsExamVM, Guid schoolid)
        {


            try
            {
                foreach (var studentExamVM in studentsExamVM)
                {
                    var student = db.Students.Where(x => x.FirstName == studentExamVM.FirstName &&
                                                         x.SecondName == studentExamVM.SecondName &&
                                                         x.SurName == studentExamVM.SurName)
                        .FirstOrDefault();
                    
                    var exam = db.Exams.Where(x => x.Name == studentExamVM.Exam).FirstOrDefault();

                    if (student == null || exam == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotAcceptable,
                            "Exam or Student have not been created. Please create the students or exams before uploading their data.");
                    }

                    ExamScore examScore = new ExamScore()
                    {
                        ID = Guid.NewGuid(),
                        ExamId = exam.ID,
                        Grade = studentExamVM.Grade,
                        Position = studentExamVM.Position,
                        SchoolId = schoolid,
                        Score = studentExamVM.Score,
                        StudentId = student.ID,
                        TeacherRemarks = studentExamVM.TeacherRemarks,
                        Term = studentExamVM.Term,
                        Title = studentExamVM.Title,
                        TotalScore = studentExamVM.TotalScore,
                        TotalStudents = studentExamVM.TotalStudents,
                        Year = studentExamVM.Year,
                        Timestamp = DateTime.Now
                    };
                    db.ExamScores.Add(examScore);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, studentsExamVM);
        }

        // POST: api/ExamScores
        [Authorize]
        [ResponseType(typeof(ExamScore))]
        public async Task<IHttpActionResult> PostExamScore(ExamScoreVM examScoreVM)
        {
            ExamScore examScore = new ExamScore()
            {
                ID = Guid.NewGuid(),
                ExamId = examScoreVM.ExamId,
                Grade = examScoreVM.Grade,
                Position = examScoreVM.Position,
                SchoolId = examScoreVM.SchoolId,
                Score = examScoreVM.Score,
                StudentId = examScoreVM.StudentId,
                TeacherRemarks = examScoreVM.TeacherRemarks,
                Term = examScoreVM.Term,
                Title = examScoreVM.Title,
                TotalScore = examScoreVM.TotalScore,
                TotalStudents = examScoreVM.TotalStudents,
                Year = examScoreVM.Year,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ExamScores.Add(examScore);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ExamScoreExists(examScore.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = examScore.ID }, examScore);
        }

        // DELETE: api/ExamScores/5
        [Authorize]
        [ResponseType(typeof(ExamScore))]
        public async Task<IHttpActionResult> DeleteExamScore(Guid id)
        {
            ExamScore examScore = await db.ExamScores.FindAsync(id);
            if (examScore == null)
            {
                return NotFound();
            }

            db.ExamScores.Remove(examScore);
            await db.SaveChangesAsync();

            return Ok(examScore);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool ExamScoreExists(Guid id)
        {
            return db.ExamScores.Count(e => e.ID == id) > 0;
        }
    }
}