﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Model.Shared;
using ElimuShare.Data.Portal;
using ElimuShare.Data.ViewModel;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;
using ElimuShare.Services.HashService;

namespace ElimuShare.Controllers
{
    public class StudentsController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        private HashService _hashService = new HashService();

        // GET: api/Students
        [Authorize]
        public IQueryable<Student> GetStudents()
        {
            return db.Students;
        }
        [Authorize]
        // GET: api/Students/5
        [ResponseType(typeof(Student))]
        public async Task<IHttpActionResult> GetStudent(Guid id)
        {
            Student student = await db.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }
        [Authorize]
        // GET: api/StudentsByParentId/5
        [HttpGet]
        [Route("api/StudentsByParentId")]
        [ResponseType(typeof(StudentByParent))]
        public HttpResponseMessage StudentsByParentId(Guid ParentId)
        {
            var students = db.Students.Where(p => p.ParentId == ParentId).ToList();
            var studentList = new List<StudentByParent>();
            foreach (var student in students)
            {
                var school = db.Schools.Find(student.SchoolId);
                if (school != null)
                {
                    var studentByParent = new StudentByParent()
                    {
                        SchoolId = student.SchoolId,
                        Timestamp = student.Timestamp,
                        ID = student.ID,
                        ImgUrl = student.ImgUrl,
                        ClassLevel = student.ClassLevel,
                        ParentId = student.ParentId,
                        SchoolName = school.Name,
                        FirstName = student.FirstName,
                        SecondName = student.SecondName,
                        SurName = student.SurName
                    };
                    studentList.Add(studentByParent);
                }
                
            }
            
            return studentList.Count == 0 ? Request.CreateResponse(HttpStatusCode.NotFound, "No students found") : Request.CreateResponse(HttpStatusCode.OK, studentList);
        }
        [Authorize]
        // GET: api/StudentsBySchoolId/5
        [HttpGet]
        [Route("api/StudentsBySchoolId")]
        [ResponseType(typeof(Student))]
        public HttpResponseMessage StudentsBySchoolId(Guid SchoolId)
        {
            var students = db.Students.Where(p => p.SchoolId == SchoolId).ToList();
            return students.Count == 0 ? Request.CreateResponse(HttpStatusCode.NotFound, "No students found") : Request.CreateResponse(HttpStatusCode.OK, students);
        }
        [Authorize]
        // GET: api/DxStudentsBySchoolId/5
        [HttpGet]
        [Route("api/DxStudentsBySchoolId")]
        [ResponseType(typeof(DxStudent))]
        public HttpResponseMessage DxStudentsBySchoolId(Guid SchoolId)
        {
            var query = ( from parents in db.Parents
                join students in db.Students on parents.ID equals students.ParentId
                where  students.SchoolId == SchoolId
                select new DxStudent
                {
                    ID = students.ID, 
                    SchoolId = students.SchoolId,
                    Parent = parents.FirstName + " " + parents.SurName,
                    ClassLevel = students.ClassLevel,
                    FirstName = students.FirstName,
                    ImgUrl = students.ImgUrl,
                    SurName = students.SurName,
                    SecondName = students.SecondName,
                    Timestamp = students.Timestamp
                }).Distinct().ToList();
            return query.Count == 0 ? Request.CreateResponse(HttpStatusCode.NotFound, "No students found") : Request.CreateResponse(HttpStatusCode.OK, query);
        }
        [Authorize]
        // PUT: api/Students/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStudent(Guid id, StudentVM studentVM)
        {
            
            Student student = new Student()
            {
                ID = id,
                ClassLevel = studentVM.ClassLevel,
                FirstName = studentVM.FirstName,
                SecondName = studentVM.SecondName,
                SurName = studentVM.SurName,
                ImgUrl = studentVM.ImgUrl,
                ParentId = studentVM.ParentId,
                SchoolId = studentVM.SchoolId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != student.ID)
            {
                return BadRequest();
            }

            db.Entry(student).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/MultipleStudents
        [HttpPost]
        [Route("api/MultipleStudents")]
        [ResponseType(typeof(List<ParentStudentVM>))]
        public HttpResponseMessage MultipleStudents(List<ParentStudentVM> parentStudentsVM, Guid SchoolId)
        {
            
           
            try
            {
                
                foreach (var parentStudentVM in parentStudentsVM)
                {
                    parentStudentVM.SchoolId = SchoolId;
                    Guid id = Guid.NewGuid();
                    byte[] hashed = _hashService.ComputeHashByte(id.ToString().Replace(@"-", ""), "1234");
                    Parent parent = new Parent()
                    {
                        ID = Guid.NewGuid(),
                        FirstName = parentStudentVM.ParentFirstName,
                        SecondName = parentStudentVM.SecondName,
                        SurName = parentStudentVM.SurName,
                        MobileNumber = parentStudentVM.ParentMobileNumber,
                        County = new County()
                        {
                            Name = "Not set",
                            Code = 0
                        },
                        Email = "Not set",
                        Gender = "Not set",
                        ImgUrl = "Not set",
                        Password = hashed,
                        Location = new Location()
                        {
                            Name = "Not set",
                            Latitude = 1,
                            Longitude = 1
                        },
                        PIN = "1234",
                        Timestamp = DateTime.Now

                    };
                    Student student = new Student()
                    {
                        ID = Guid.NewGuid(),
                        ClassLevel = parentStudentVM.ClassLevel,
                        FirstName = parentStudentVM.FirstName,
                        SecondName = parentStudentVM.SecondName,
                        SurName = parentStudentVM.SurName,
                        ImgUrl = "Not available",
                        ParentId = parent.ID,
                        SchoolId = parentStudentVM.SchoolId,
                        Timestamp = DateTime.Now
                    };
                    db.Parents.Add(parent);
                    db.Students.Add(student);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, parentStudentsVM);
        }
        [Authorize]
        // POST: api/Students
        [ResponseType(typeof(Student))]
        public async Task<IHttpActionResult> PostStudent(StudentVM studentVM)
        {
            Student student = new Student()
            {
                ID = Guid.NewGuid(),
                ClassLevel = studentVM.ClassLevel,
                FirstName = studentVM.FirstName,
                SecondName = studentVM.SecondName,
                SurName = studentVM.SurName,
                ImgUrl = studentVM.ImgUrl,
                ParentId = studentVM.ParentId,
                SchoolId = studentVM.SchoolId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Students.Add(student);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (StudentExists(student.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = student.ID }, student);
        }
        [Authorize]
        // DELETE: api/Students/5
        [ResponseType(typeof(Student))]
        public async Task<IHttpActionResult> DeleteStudent(Guid id)
        {
            Student student = await db.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            db.Students.Remove(student);
            await db.SaveChangesAsync();

            return Ok(student);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool StudentExists(Guid id)
        {
            return db.Students.Count(e => e.ID == id) > 0;
        }
    }
}