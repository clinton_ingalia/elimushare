﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.ViewModel;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class UserAccessRolesController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();

        // GET: api/UserAccessRoles
        public IQueryable<UserAccessRole> GetUserAccessRoles()
        {
            return db.UserAccessRoles;
        }

        // GET: api/UserAccessRoles/5
        [ResponseType(typeof(UserAccessRole))]
        public async Task<IHttpActionResult> GetUserAccessRole(Guid id)
        {
            UserAccessRole userAccessRole = await db.UserAccessRoles.FindAsync(id);
            if (userAccessRole == null)
            {
                return NotFound();
            }

            return Ok(userAccessRole);
        }

        // PUT: api/UserAccessRoles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserAccessRole(Guid id, UserRoleVM userAccessRoleVm)
        {
            var userAccessRole = new UserAccessRole()
            {
                ID = Guid.NewGuid(),
                Role = userAccessRoleVm.Role,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userAccessRole.ID)
            {
                return BadRequest();
            }

            db.Entry(userAccessRole).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserAccessRoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserAccessRoles
        [ResponseType(typeof(UserAccessRole))]
        public async Task<IHttpActionResult> PostUserAccessRole(UserRoleVM userAccessRoleVm)
        {
            var userAccessRole = new UserAccessRole()
            {
                ID = Guid.NewGuid(),
                Role = userAccessRoleVm.Role,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserAccessRoles.Add(userAccessRole);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserAccessRoleExists(userAccessRole.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = userAccessRole.ID }, userAccessRole);
        }

        // DELETE: api/UserAccessRoles/5
        [ResponseType(typeof(UserAccessRole))]
        public async Task<IHttpActionResult> DeleteUserAccessRole(Guid id)
        {
            UserAccessRole userAccessRole = await db.UserAccessRoles.FindAsync(id);
            if (userAccessRole == null)
            {
                return NotFound();
            }

            db.UserAccessRoles.Remove(userAccessRole);
            await db.SaveChangesAsync();

            return Ok(userAccessRole);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserAccessRoleExists(Guid id)
        {
            return db.UserAccessRoles.Count(e => e.ID == id) > 0;
        }
    }
}