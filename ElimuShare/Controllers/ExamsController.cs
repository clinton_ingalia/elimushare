﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Model.Shared;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    /// <summary>
    /// Exams api controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ExamsController : ApiController
    {
        /// <summary>
        /// The database
        /// </summary>
        private ElimuShareContext db = new ElimuShareContext();

        // GET: api/Exams
        /// <summary>
        /// Gets the exams.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IQueryable<Exam> GetExams()
        {
            return db.Exams;
        }

        // GET: api/ExamsBySchool
        /// <summary>
        /// Examses the by school.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("api/ExamsBySchool")]
        public IQueryable<Exam> ExamsBySchool(Guid Id)
        {
            return db.Exams.Where(x => x.SchoolId == Id);
        }

        // GET: api/Exams/5
        /// <summary>
        /// Gets the exam.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Authorize]
        [ResponseType(typeof(Exam))]
        public async Task<IHttpActionResult> GetExam(Guid id)
        {
            Exam exam = await db.Exams.FindAsync(id);
            if (exam == null)
            {
                return NotFound();
            }

            return Ok(exam);
        }

        //// GET: api/ExamsByStudentId
        //[HttpGet]
        //[Route("api/ExamsByStudentId")]
        //public IQueryable<Exam> ExamsByStudentId(Guid StudentId)
        //{
        //    return db.Exams.Where(x => x.StudentId == StudentId);
        //}

        // PUT: api/Exams/5
        /// <summary>
        /// Puts the exam.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="examVM">The exam vm.</param>
        /// <returns></returns>
        [Authorize]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutExam(Guid id, ExamVM examVM)
        {
            var exam = new Exam()
            {
                ID = id,
                Description = examVM.Description,
                Name = examVM.Name,
                Term = examVM.Term,
                Year = examVM.Year,
                SchoolId = examVM.SchoolId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != exam.ID)
            {
                return BadRequest();
            }

            db.Entry(exam).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExamExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MultipleExams
        /// <summary>
        /// Multiples the exams.
        /// </summary>
        /// <param name="examsVM">The exams vm.</param>
        /// <param name="SchoolId">The school identifier.</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("api/MultipleExams")]
        [ResponseType(typeof(List<Exam>))]
        public HttpResponseMessage MultipleExams(List<ExamVM> examsVM, Guid SchoolId)
        {


            try
            {
                foreach (var examVM in examsVM)
                {
                    Exam exam = new Exam()
                    {
                        ID = Guid.NewGuid(),
                        Description = examVM.Description,
                        Name = examVM.Name,
                        Term = examVM.Term,
                        Year = examVM.Year,
                        SchoolId = SchoolId,
                        Timestamp = DateTime.Now
                    };
                    db.Exams.Add(exam);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, examsVM);
        }

        // POST: api/Exams
        /// <summary>
        /// Posts the exam.
        /// </summary>
        /// <param name="examVM">The exam vm.</param>
        /// <returns></returns>
        [Authorize]
        [ResponseType(typeof(Exam))]
        public async Task<IHttpActionResult> PostExam(ExamVM examVM)
        {
            var exam = new Exam()
            {
                ID = Guid.NewGuid(),
                Description = examVM.Description,
                Name = examVM.Name,
                Term = examVM.Term,
                Year = examVM.Year,
                SchoolId = examVM.SchoolId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Exams.Add(exam);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ExamExists(exam.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = exam.ID }, exam);
        }

        // DELETE: api/Exams/5
        /// <summary>
        /// Deletes the exam.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Authorize]
        [ResponseType(typeof(Exam))]
        public async Task<IHttpActionResult> DeleteExam(Guid id)
        {
            Exam exam = await db.Exams.FindAsync(id);
            if (exam == null)
            {
                return NotFound();
            }

            db.Exams.Remove(exam);
            await db.SaveChangesAsync();

            return Ok(exam);
        }

        /// <summary>
        /// Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Exams the exists.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Authorize]
        private bool ExamExists(Guid id)
        {
            return db.Exams.Count(e => e.ID == id) > 0;
        }
    }
}