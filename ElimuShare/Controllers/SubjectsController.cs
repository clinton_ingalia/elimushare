﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Model.Shared;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Models;

namespace ElimuShare.Controllers
{
    public class SubjectsController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();

        // GET: api/Subjects
        [Authorize]
        public IQueryable<Subject> GetSubjects()
        {
            return db.Subjects;
        }
        [Authorize]
        // GET: api/SubjectsBySchool
        [HttpGet]
        [Route("api/SubjectsBySchool")]
        public IQueryable<Subject> SubjectsBySchool(Guid Id)
        {
            return db.Subjects.Where(x => x.SchoolId == Id);
        }
        [Authorize]
        // GET: api/Subjects/5
        [ResponseType(typeof(Subject))]
        public async Task<IHttpActionResult> GetSubject(Guid id)
        {
            Subject subject = await db.Subjects.FindAsync(id);
            if (subject == null)
            {
                return NotFound();
            }

            return Ok(subject);
        }
        [Authorize]
        // PUT: api/Subjects/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSubject(Guid id, SubjectVM subjectVM)
        {
            var subject = new Subject()
            {
                ID = id,
                Description = subjectVM.Description,
                Name = subjectVM.Name,
                SchoolId = subjectVM.SchoolId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subject.ID)
            {
                return BadRequest();
            }

            db.Entry(subject).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/MultipleSubjects
        [HttpPost]
        [Route("api/MultipleSubjects")]
        [ResponseType(typeof(List<Subject>))]
        public HttpResponseMessage MultipleSubjects(List<SubjectVM> subjectsVM, Guid SchoolId)
        {


            try
            {
                foreach (var subjectVM in subjectsVM)
                {
                    Subject subject = new Subject()
                    {
                        ID = Guid.NewGuid(),
                        Description = subjectVM.Description,
                        Name = subjectVM.Name,
                        SchoolId = SchoolId,
                        Timestamp = DateTime.Now
                    };
                    db.Subjects.Add(subject);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, subjectsVM);
        }
        [Authorize]
        // POST: api/Subjects
        [ResponseType(typeof(Subject))]
        public async Task<IHttpActionResult> PostSubject(SubjectVM subjectVM)
        {
            var subject = new Subject()
            {
                ID = Guid.NewGuid(),
                Description = subjectVM.Description,
                Name = subjectVM.Name,
                SchoolId = subjectVM.SchoolId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Subjects.Add(subject);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SubjectExists(subject.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = subject.ID }, subject);
        }
        [Authorize]
        // DELETE: api/Subjects/5
        [ResponseType(typeof(Subject))]
        public async Task<IHttpActionResult> DeleteSubject(Guid id)
        {
            Subject subject = await db.Subjects.FindAsync(id);
            if (subject == null)
            {
                return NotFound();
            }

            db.Subjects.Remove(subject);
            await db.SaveChangesAsync();

            return Ok(subject);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool SubjectExists(Guid id)
        {
            return db.Subjects.Count(e => e.ID == id) > 0;
        }
    }
}