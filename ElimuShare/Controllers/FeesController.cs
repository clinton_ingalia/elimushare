﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Portal;
using ElimuShare.Models;
using ElimuShare.Data.ViewModel.Common;

namespace ElimuShare.Controllers
{
    public class FeesController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();

        // GET: api/Fees
        [Authorize]
        public IQueryable<Fee> GetFees()
        {
            return db.Fees;
        }

        // GET: api/Fees/5
        [Authorize]
        [ResponseType(typeof(Fee))]
        public async Task<IHttpActionResult> GetFee(Guid id)
        {
            Fee fee = await db.Fees.FindAsync(id);
            if (fee == null)
            {
                return NotFound();
            }

            return Ok(fee);
        }

        // GET: api/FeesByStudentId
        [Authorize]
        [HttpGet]
        [Route("api/FeesByStudentId")]
        public IQueryable<Fee> FeesByStudentId(Guid StudentId)
        {
            return db.Fees.Where(x => x.StudentId == StudentId);
        }

        // GET: api/FeesBySchoolId
        [Authorize]
        [HttpGet]
        [Route("api/FeesBySchoolId")]
        public IQueryable<Fee> FeesBySchoolId(Guid SchoolId)
        {
            return db.Fees.Where(x => x.SchoolId == SchoolId);
        }

        // GET: api/LatestFeesBySchoolId
        [Authorize]
        [HttpGet]
        [Route("api/LatestFeesBySchoolId")]
        public HttpResponseMessage LatestFeesBySchoolId(Guid SchoolId)
        {
            var query = (from f in db.Fees
                         join s in db.Students on f.StudentId equals s.ID
                         where f.SchoolId == SchoolId
                         join p in db.PaymentMethods on f.PaymentMethodId equals p.ID
                         select new LatestFeeDashboard()
                         {
                             ID = f.ID,
                             StudentName = s.FirstName + " " + s.SurName,
                             Amount = f.Amount,
                             Timestamp = f.Timestamp,
                             Term = f.Term,
                             Paid = f.Paid,
                             Year = f.Year,
                             PaymentMethod = p.Name,
                             Balance = f.Balance
                         }).ToList().OrderByDescending(x => x.Timestamp).Take(10);
            return query.Any() ? Request.CreateResponse(HttpStatusCode.OK, query) : Request.CreateResponse(HttpStatusCode.NotFound, "No data found");
        }

        // GET: api/DxFeesBySchoolId
        [Authorize]
        [HttpGet]
        [Route("api/DxFeesBySchoolId")]
        public HttpResponseMessage DxFeesBySchoolId(Guid SchoolId)
        {
            var query = (from f in db.Fees
                         join s in db.Students on f.StudentId equals s.ID
                         where f.SchoolId == SchoolId
                         join p in db.PaymentMethods on f.PaymentMethodId equals p.ID
                         select new LatestFeeDashboard()
                         {
                             ID = f.ID,
                             StudentName = s.FirstName + " " + s.SurName,
                             Amount = f.Amount,
                             Timestamp = f.Timestamp,
                             Term = f.Term,
                             Paid = f.Paid,
                             Year = f.Year,
                             PaymentMethod = p.Name,
                             Balance = f.Balance
                         }).ToList().OrderByDescending(x => x.Timestamp);
            return query.Any() ? Request.CreateResponse(HttpStatusCode.OK, query) : Request.CreateResponse(HttpStatusCode.NotFound, "No data found");
        }

        // PUT: api/Fees/5
        [Authorize]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFee(Guid id, FeeVM feeVM)
        {
            Fee fee = new Fee()
            {
                ID = id,
                SchoolId = feeVM.SchoolId,
                StudentId = feeVM.StudentId,
                Amount = feeVM.Amount,
                Paid = feeVM.Paid,
                Balance = feeVM.Balance,
                PaymentMethodId = feeVM.PaymentMethodId,
                Term = feeVM.Term,
                Year = feeVM.Year,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fee.ID)
            {
                return BadRequest();
            }

            db.Entry(fee).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MultipleFees
        [Authorize]
        [HttpPost]
        [Route("api/MultipleFees")]
        [ResponseType(typeof(List<Fee>))]
        public HttpResponseMessage MultipleFees(List<FeeUploaderVM> feesVM, Guid SchoolId)
        {
            try
            {
                var school = db.Schools.Where(x => x.ID == SchoolId).FirstOrDefault();
                if (school != null)
                {
                    foreach (var feeVM in feesVM)
                    {
                        var student = db.Students
                            .Where(x => x.FirstName == feeVM.FirstName && x.SecondName == feeVM.SecondName &&
                                        x.SurName == feeVM.SurName)
                            .FirstOrDefault();
                        var paymentMethod = db.PaymentMethods.Where(x => x.Name == feeVM.PaymentMethod)
                            .FirstOrDefault();
                        if (student != null && paymentMethod != null)
                        {
                            Fee fee = new Fee()
                            {
                                ID = Guid.NewGuid(),
                                SchoolId = school.ID,
                                StudentId = student.ID,
                                Amount = feeVM.Amount,
                                Balance = feeVM.Balance,
                                Paid = feeVM.Paid,
                                PaymentMethodId = paymentMethod.ID,
                                Term = feeVM.Term,
                                Year = feeVM.Year,
                                Timestamp = DateTime.Now
                            };
                            db.Fees.Add(fee);
                        }

                    }
                    db.SaveChanges();
                }


            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, feesVM);
        }

        // POST: api/Fees
        [Authorize]
        [ResponseType(typeof(Fee))]
        public async Task<IHttpActionResult> PostFee(FeeVM feeVM)
        {
            Fee fee = new Fee()
            {
                ID = Guid.NewGuid(),
                SchoolId = feeVM.SchoolId,
                StudentId = feeVM.StudentId,
                Amount = feeVM.Amount,
                Balance = feeVM.Balance,
                Paid = feeVM.Paid,
                PaymentMethodId = feeVM.PaymentMethodId,
                Term = feeVM.Term,
                Year = feeVM.Year,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Fees.Add(fee);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FeeExists(fee.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = fee.ID }, fee);
        }

        // DELETE: api/Fees/5
        [Authorize]
        [ResponseType(typeof(Fee))]
        public async Task<IHttpActionResult> DeleteFee(Guid id)
        {
            Fee fee = await db.Fees.FindAsync(id);
            if (fee == null)
            {
                return NotFound();
            }

            db.Fees.Remove(fee);
            await db.SaveChangesAsync();

            return Ok(fee);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool FeeExists(Guid id)
        {
            return db.Fees.Count(e => e.ID == id) > 0;
        }
    }
}