﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ElimuShare.Data.Model;
using ElimuShare.Data.Model.Shared;
using ElimuShare.Models;
using ElimuShare.Data.ViewModel;
using ElimuShare.Data.ViewModel.Common;
using ElimuShare.Services.HashService;
using ElimuShare.Services.SMSService;

namespace ElimuShare.Controllers
{
    public class ParentsController : ApiController
    {
        private ElimuShareContext db = new ElimuShareContext();
        private ParentRepository repo = new ParentRepository();
        private HashService _hashService = new HashService();

        [Authorize]
        // GET: api/Parents
        public IQueryable<Parent> GetParents()
        {
            return db.Parents;
        }
        [Authorize]
        // GET: api/ParentsBySchoolId/5
        [HttpGet]
        [Route("api/ParentsBySchoolId")]
        [ResponseType(typeof(Parent))]
        public HttpResponseMessage ParentsBySchoolId(Guid SchoolId)
        {
            var students = db.Students.Where(p => p.SchoolId == SchoolId).ToList();
            var parents = db.Parents.ToList();
            var parentsList = new List<Parent>();

            foreach (var student in students)
            {
                foreach (var parent in parents.ToList())
                {
                    if (student.ParentId == parent.ID)
                    {
                        parentsList.Add(parent);
                    }
                }
            }
            var listWithNoDuplicates = parentsList.Distinct().ToList();
            return parentsList.Count == 0 ? Request.CreateResponse(HttpStatusCode.NotFound, "No parents found") : Request.CreateResponse(HttpStatusCode.OK, listWithNoDuplicates);
        }
        [Authorize]
        // GET: api/Parents/5
        [ResponseType(typeof(Parent))]
        public async Task<IHttpActionResult> GetParent(Guid id)
        {
            Parent parent = await db.Parents.FindAsync(id);
            if (parent == null)
            {
                return NotFound();
            }

            return Ok(parent);
        }
        [Authorize]
        [HttpPut]
        [Route("api/EditParentBySchool")]
        public HttpResponseMessage EditParentBySchool(Guid id, ParentEditSchoolVM parentEditSchoolVM)
        {
            var oldParent = db.Parents.Where(x => x.ID == id).FirstOrDefault();
            if (oldParent != null)
            {
                oldParent.MobileNumber = parentEditSchoolVM.MobileNumber;
                oldParent.FirstName = parentEditSchoolVM.FirstName;
                oldParent.SecondName = parentEditSchoolVM.SecondName;
                oldParent.SurName = parentEditSchoolVM.SurName;
                oldParent.Gender = parentEditSchoolVM.Gender;

                db.Entry(oldParent).State = EntityState.Modified;
                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, oldParent);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotModified, parentEditSchoolVM);
            }
        }
        [Authorize]
        // PUT: api/Parents/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutParent(Guid id, ParentVM parentVM)
        {
            Guid newId = id;
            byte[] hashed = _hashService.ComputeHashByte(newId.ToString().Replace(@"-", ""), parentVM.Password);
            Parent parent = new Parent()
            {
                ID = newId,
                Email = parentVM.Email,
                FirstName = parentVM.FirstName,
                SecondName = parentVM.SecondName,
                SurName = parentVM.SurName,
                ImgUrl = parentVM.ImgUrl,
                Location = parentVM.Location,
                Gender = parentVM.Gender,
                MobileNumber = parentVM.MobileNumber,
                PIN = parentVM.PIN,
                Password = hashed,
                County = parentVM.County,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parent.ID)
            {
                return BadRequest();
            }

            db.Entry(parent).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize]
        // POST: api/MultipleParents
        [HttpPost]
        [Route("api/MultipleParents")]
        [ResponseType(typeof(List<Parent>))]
        public HttpResponseMessage MultipleParents(List<ParentVM> parentsVM)
        {


            try
            {
                foreach (var parentVM in parentsVM)
                {
                    Guid newId = Guid.NewGuid();
                    byte[] hashed = _hashService.ComputeHashByte(newId.ToString().Replace(@"-", ""), parentVM.Password);
                    Parent parent = new Parent()
                    {
                        ID = newId,
                        FirstName = parentVM.FirstName,
                        SecondName = parentVM.SecondName,
                        SurName = parentVM.SurName,
                        MobileNumber = parentVM.MobileNumber,
                        Password = hashed,
                        Gender = parentVM.Gender,
                        County = new County()
                        {
                            Name = "Not Set",
                            Code = 00
                        },
                        Email = "Not@Set.com",
                        ImgUrl = "NoImageSet",
                        Location = new Location()
                        {
                            Name = "NotSet",
                            Latitude = 2,
                            Longitude = 1
                        },
                        PIN = "1234",
                        Timestamp = DateTime.Now
                    };
                    db.Parents.Add(parent);
                }
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, parentsVM);
        }
        [Authorize]
        //POST: api/ParentsBySchool
        [HttpPost]
        [Route("api/ParentsBySchool")]
        public HttpResponseMessage ParentsBySchool(ParentRegisterVM parentVM)
        {
            Guid newId = Guid.NewGuid();
            byte[] hashed = _hashService.ComputeHashByte(newId.ToString().Replace(@"-", ""), parentVM.Password);
            var parent = new Parent();
            parent = new Parent()
            {
                ID = newId,
                FirstName = parentVM.FirstName,
                SecondName = parentVM.SecondName,
                SurName = parentVM.SurName,
                MobileNumber = parentVM.MobileNumber,
                Password = hashed,
                Gender = parentVM.Gender,
                County = new County()
                {
                    Name = "Not Set",
                    Code = 00
                },
                Email = "Not@Set.com",
                ImgUrl = "NoImageSet",
                Location = new Location()
                {
                    Name = "NotSet",
                    Latitude = 2,
                    Longitude = 1
                },
                PIN = "1234",
                Timestamp = DateTime.Now
            };

            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, parentVM);
            }

            if (repo.IsRegistered(parent.MobileNumber))
            {
                var oldParent = db.Parents.Where(x => x.MobileNumber == parent.MobileNumber).FirstOrDefault();
                return Request.CreateResponse(HttpStatusCode.OK, oldParent);
            }
            db.Parents.Add(parent);


            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, parent);

        }
        [Authorize]
        // POST: api/Parents
        [ResponseType(typeof(Parent))]
        public async Task<IHttpActionResult> PostParent(ParentRegisterVM parentVM)
        {
            Guid newId = Guid.NewGuid();
            byte[] hashed = _hashService.ComputeHashByte(newId.ToString().Replace(@"-", ""), parentVM.Password);
            var parent = new Parent();
            parent = new Parent()
            {
                ID = newId,
                FirstName = parentVM.FirstName,
                SecondName = parentVM.SecondName,
                SurName = parentVM.SurName,
                MobileNumber = parentVM.MobileNumber,
                Password = hashed,
                Gender = parentVM.Gender,
                County = new County()
                {
                    Name = "Not Set",
                    Code = 00
                },
                Email = "Not@Set.com",
                ImgUrl = "NoImageSet",
                Location = new Location()
                {
                    Name = "NotSet",
                    Latitude = 2,
                    Longitude = 1
                },
                PIN = "1234",
                Timestamp = DateTime.Now
            };
            if (repo.IsRegistered(parentVM.MobileNumber))
            {
                var oldParent = db.Parents.AsNoTracking().Where(x => x.MobileNumber == parentVM.MobileNumber).FirstOrDefault();
                parent.ID = oldParent.ID;
                if (oldParent.Activated == false)
                {
                    parent.Activated = true;
                    parent.Password = _hashService.ComputeHashByte(parent.ID.ToString().Replace(@"-", ""), parentVM.Password);
                    //db.Parents.Attach(parent);
                    db.Entry(parent).State = EntityState.Modified;
                }
                else
                {
                    return Conflict();
                }

            }
            else
            {


                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (repo.IsRegistered(parent.MobileNumber) && parent.Activated)
                {
                    return Conflict();
                }

                db.Parents.Add(parent);

            }


            try
            {
                await db.SaveChangesAsync();
                SMS sms = new SMS();
                sms.SendSMS(parentVM.MobileNumber, "Hello " + parentVM.FirstName + ", Welcome to ElimuShare, you have successfully created an account");
            }
            catch (DbUpdateException)
            {
                if (ParentExists(parent.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = parent.ID }, parent);
        }
        [Authorize]
        // DELETE: api/Parents/5
        [ResponseType(typeof(Parent))]
        public async Task<IHttpActionResult> DeleteParent(Guid id)
        {
            Parent parent = await db.Parents.FindAsync(id);
            if (parent == null)
            {
                return NotFound();
            }

            db.Parents.Remove(parent);
            await db.SaveChangesAsync();

            return Ok(parent);
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [Authorize]
        private bool ParentExists(Guid id)
        {
            return db.Parents.Count(e => e.ID == id) > 0;
        }
    }
}