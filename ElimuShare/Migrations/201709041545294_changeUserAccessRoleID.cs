namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeUserAccessRoleID : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserAccessRoles",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Role = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.UserRoles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Role = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.UserAccessRoles");
        }
    }
}
