namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newExamModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exams", "Term", c => c.Int(nullable: false));
            AddColumn("dbo.Exams", "Year", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Exams", "Year");
            DropColumn("dbo.Exams", "Term");
        }
    }
}
