namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class paymentMethod : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PaymentMethods",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        Name = c.String(),
                        AccountName = c.String(),
                        PayBill = c.Int(nullable: false),
                        TillNumber = c.Int(nullable: false),
                        AccountNumber = c.Int(nullable: false),
                        Instruction = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SchoolType_Category = c.String(),
                        SchoolType_Boarding = c.Boolean(nullable: false),
                        SchoolType_Gender = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubjectScores",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SubjectId = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        Title = c.String(),
                        Year = c.DateTime(nullable: false),
                        Term = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                        TotalScore = c.Int(nullable: false),
                        TeacherRemarks = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SubjectScores");
            DropTable("dbo.Subjects");
            DropTable("dbo.PaymentMethods");
        }
    }
}
