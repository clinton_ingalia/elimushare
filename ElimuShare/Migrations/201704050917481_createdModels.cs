namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Location_Name = c.String(),
                        Location_Latitude = c.Single(nullable: false),
                        Location_Longitude = c.Single(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Fees",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        Amount = c.Double(nullable: false),
                        BankId = c.Guid(nullable: false),
                        ConfirmationCode = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Parents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FullNames_FirstName = c.String(),
                        FullNames_SecondName = c.String(),
                        FullNames_SurName = c.String(),
                        MobileNumber = c.String(),
                        Password = c.String(),
                        PIN = c.String(),
                        Email = c.String(),
                        Location_Name = c.String(),
                        Location_Latitude = c.Single(nullable: false),
                        Location_Longitude = c.Single(nullable: false),
                        ImgUrl = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Results",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        Title = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Schools",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        LogoUrl = c.String(),
                        ImgUrl = c.String(),
                        Location_Name = c.String(),
                        Location_Latitude = c.Single(nullable: false),
                        Location_Longitude = c.Single(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        ParentId = c.Guid(nullable: false),
                        FullNames_FirstName = c.String(),
                        FullNames_SecondName = c.String(),
                        FullNames_SurName = c.String(),
                        Location_Name = c.String(),
                        Location_Latitude = c.Single(nullable: false),
                        Location_Longitude = c.Single(nullable: false),
                        ImgUrl = c.String(),
                        ClassLevel = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserTypeId = c.Guid(nullable: false),
                        FullNames_FirstName = c.String(),
                        FullNames_SecondName = c.String(),
                        FullNames_SurName = c.String(),
                        MobileNumber = c.String(),
                        Password = c.String(),
                        ImgUrl = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Vacancies",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Number = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Vacancies");
            DropTable("dbo.Users");
            DropTable("dbo.Students");
            DropTable("dbo.Schools");
            DropTable("dbo.Results");
            DropTable("dbo.Parents");
            DropTable("dbo.Notifications");
            DropTable("dbo.Fees");
            DropTable("dbo.Events");
        }
    }
}
