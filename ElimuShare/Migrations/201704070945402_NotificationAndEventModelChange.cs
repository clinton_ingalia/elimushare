namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationAndEventModelChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "AuthorId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "AuthorId");
        }
    }
}
