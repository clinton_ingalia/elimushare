namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class schooltypeandcounty : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SchoolTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Category = c.String(),
                        Boarding = c.Boolean(nullable: false),
                        Gender = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Parents", "County_Name", c => c.String());
            AddColumn("dbo.Parents", "County_Code", c => c.Int(nullable: false));
            AddColumn("dbo.Schools", "County_Name", c => c.String());
            AddColumn("dbo.Schools", "County_Code", c => c.Int(nullable: false));
            AddColumn("dbo.Schools", "SchoolType_ID", c => c.Guid());
            AddColumn("dbo.Users", "Location_Name", c => c.String());
            AddColumn("dbo.Users", "Location_Latitude", c => c.Single(nullable: false));
            AddColumn("dbo.Users", "Location_Longitude", c => c.Single(nullable: false));
            AddColumn("dbo.Users", "County_Name", c => c.String());
            AddColumn("dbo.Users", "County_Code", c => c.Int(nullable: false));
            CreateIndex("dbo.Schools", "SchoolType_ID");
            AddForeignKey("dbo.Schools", "SchoolType_ID", "dbo.SchoolTypes", "ID");
            DropColumn("dbo.Students", "Location_Name");
            DropColumn("dbo.Students", "Location_Latitude");
            DropColumn("dbo.Students", "Location_Longitude");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "Location_Longitude", c => c.Single(nullable: false));
            AddColumn("dbo.Students", "Location_Latitude", c => c.Single(nullable: false));
            AddColumn("dbo.Students", "Location_Name", c => c.String());
            DropForeignKey("dbo.Schools", "SchoolType_ID", "dbo.SchoolTypes");
            DropIndex("dbo.Schools", new[] { "SchoolType_ID" });
            DropColumn("dbo.Users", "County_Code");
            DropColumn("dbo.Users", "County_Name");
            DropColumn("dbo.Users", "Location_Longitude");
            DropColumn("dbo.Users", "Location_Latitude");
            DropColumn("dbo.Users", "Location_Name");
            DropColumn("dbo.Schools", "SchoolType_ID");
            DropColumn("dbo.Schools", "County_Code");
            DropColumn("dbo.Schools", "County_Name");
            DropColumn("dbo.Parents", "County_Code");
            DropColumn("dbo.Parents", "County_Name");
            DropTable("dbo.SchoolTypes");
        }
    }
}
