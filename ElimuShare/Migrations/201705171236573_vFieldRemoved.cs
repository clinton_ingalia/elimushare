namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vFieldRemoved : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Events", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.ExamScores", "ExamId", "dbo.Exams");
            DropForeignKey("dbo.ExamScores", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.ExamScores", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Fees", "PaymentMethodId", "dbo.PaymentMethods");
            DropForeignKey("dbo.Fees", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.Fees", "StudentId", "dbo.Students");
            DropForeignKey("dbo.SubjectScores", "ExamId", "dbo.Exams");
            DropForeignKey("dbo.SubjectScores", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.SubjectScores", "SubjectId", "dbo.Subjects");
            DropIndex("dbo.Events", new[] { "SchoolId" });
            DropIndex("dbo.ExamScores", new[] { "StudentId" });
            DropIndex("dbo.ExamScores", new[] { "ExamId" });
            DropIndex("dbo.ExamScores", new[] { "SchoolId" });
            DropIndex("dbo.Fees", new[] { "StudentId" });
            DropIndex("dbo.Fees", new[] { "SchoolId" });
            DropIndex("dbo.Fees", new[] { "PaymentMethodId" });
            DropIndex("dbo.SubjectScores", new[] { "SubjectId" });
            DropIndex("dbo.SubjectScores", new[] { "ExamId" });
            DropIndex("dbo.SubjectScores", new[] { "SchoolId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.SubjectScores", "SchoolId");
            CreateIndex("dbo.SubjectScores", "ExamId");
            CreateIndex("dbo.SubjectScores", "SubjectId");
            CreateIndex("dbo.Fees", "PaymentMethodId");
            CreateIndex("dbo.Fees", "SchoolId");
            CreateIndex("dbo.Fees", "StudentId");
            CreateIndex("dbo.ExamScores", "SchoolId");
            CreateIndex("dbo.ExamScores", "ExamId");
            CreateIndex("dbo.ExamScores", "StudentId");
            CreateIndex("dbo.Events", "SchoolId");
            AddForeignKey("dbo.SubjectScores", "SubjectId", "dbo.Subjects", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SubjectScores", "SchoolId", "dbo.Schools", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SubjectScores", "ExamId", "dbo.Exams", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Fees", "StudentId", "dbo.Students", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Fees", "SchoolId", "dbo.Schools", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Fees", "PaymentMethodId", "dbo.PaymentMethods", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ExamScores", "StudentId", "dbo.Students", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ExamScores", "SchoolId", "dbo.Schools", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ExamScores", "ExamId", "dbo.Exams", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Events", "SchoolId", "dbo.Schools", "ID", cascadeDelete: true);
        }
    }
}
