namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vacancyResponseModel2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VacancyResponses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        VacancyId = c.Guid(nullable: false),
                        ParentNames = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        StudentNames = c.String(),
                        PreviousSchoolName = c.String(),
                        Qualifications = c.String(),
                        ChildDescription = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.VacancyResponses");
        }
    }
}
