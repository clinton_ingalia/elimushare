namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vFields : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Fees", "StudentId");
            CreateIndex("dbo.Fees", "SchoolId");
            CreateIndex("dbo.SubjectScores", "SubjectId");
            CreateIndex("dbo.SubjectScores", "ExamId");
            CreateIndex("dbo.SubjectScores", "SchoolId");
            CreateIndex("dbo.SubjectScores", "StudentId");
            AddForeignKey("dbo.Fees", "SchoolId", "dbo.Schools", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Fees", "StudentId", "dbo.Students", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SubjectScores", "ExamId", "dbo.Exams", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SubjectScores", "SchoolId", "dbo.Schools", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SubjectScores", "StudentId", "dbo.Students", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SubjectScores", "SubjectId", "dbo.Subjects", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubjectScores", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.SubjectScores", "StudentId", "dbo.Students");
            DropForeignKey("dbo.SubjectScores", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.SubjectScores", "ExamId", "dbo.Exams");
            DropForeignKey("dbo.Fees", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Fees", "SchoolId", "dbo.Schools");
            DropIndex("dbo.SubjectScores", new[] { "StudentId" });
            DropIndex("dbo.SubjectScores", new[] { "SchoolId" });
            DropIndex("dbo.SubjectScores", new[] { "ExamId" });
            DropIndex("dbo.SubjectScores", new[] { "SubjectId" });
            DropIndex("dbo.Fees", new[] { "SchoolId" });
            DropIndex("dbo.Fees", new[] { "StudentId" });
        }
    }
}
