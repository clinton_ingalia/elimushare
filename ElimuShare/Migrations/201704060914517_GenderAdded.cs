namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GenderAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Parents", "Gender", c => c.String());
            AddColumn("dbo.Users", "Gender", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Gender");
            DropColumn("dbo.Parents", "Gender");
        }
    }
}
