namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fullnamesChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Parents", "FirstName", c => c.String());
            AddColumn("dbo.Parents", "SecondName", c => c.String());
            AddColumn("dbo.Parents", "SurName", c => c.String());
            AddColumn("dbo.Students", "FirstName", c => c.String());
            AddColumn("dbo.Students", "SecondName", c => c.String());
            AddColumn("dbo.Students", "SurName", c => c.String());
            AddColumn("dbo.Users", "FirstName", c => c.String());
            AddColumn("dbo.Users", "SecondName", c => c.String());
            AddColumn("dbo.Users", "SurName", c => c.String());
            DropColumn("dbo.Parents", "FullNames_FirstName");
            DropColumn("dbo.Parents", "FullNames_SecondName");
            DropColumn("dbo.Parents", "FullNames_SurName");
            DropColumn("dbo.Students", "FullNames_FirstName");
            DropColumn("dbo.Students", "FullNames_SecondName");
            DropColumn("dbo.Students", "FullNames_SurName");
            DropColumn("dbo.Users", "FullNames_FirstName");
            DropColumn("dbo.Users", "FullNames_SecondName");
            DropColumn("dbo.Users", "FullNames_SurName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "FullNames_SurName", c => c.String());
            AddColumn("dbo.Users", "FullNames_SecondName", c => c.String());
            AddColumn("dbo.Users", "FullNames_FirstName", c => c.String());
            AddColumn("dbo.Students", "FullNames_SurName", c => c.String());
            AddColumn("dbo.Students", "FullNames_SecondName", c => c.String());
            AddColumn("dbo.Students", "FullNames_FirstName", c => c.String());
            AddColumn("dbo.Parents", "FullNames_SurName", c => c.String());
            AddColumn("dbo.Parents", "FullNames_SecondName", c => c.String());
            AddColumn("dbo.Parents", "FullNames_FirstName", c => c.String());
            DropColumn("dbo.Users", "SurName");
            DropColumn("dbo.Users", "SecondName");
            DropColumn("dbo.Users", "FirstName");
            DropColumn("dbo.Students", "SurName");
            DropColumn("dbo.Students", "SecondName");
            DropColumn("dbo.Students", "FirstName");
            DropColumn("dbo.Parents", "SurName");
            DropColumn("dbo.Parents", "SecondName");
            DropColumn("dbo.Parents", "FirstName");
        }
    }
}
