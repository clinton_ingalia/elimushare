namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class examAndsSubject : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exams", "SchoolId", c => c.Guid(nullable: false));
            AddColumn("dbo.Subjects", "SchoolId", c => c.Guid(nullable: false));
            DropColumn("dbo.Exams", "SchoolType_Category");
            DropColumn("dbo.Exams", "SchoolType_Boarding");
            DropColumn("dbo.Exams", "SchoolType_Gender");
            DropColumn("dbo.Subjects", "SchoolType_Category");
            DropColumn("dbo.Subjects", "SchoolType_Boarding");
            DropColumn("dbo.Subjects", "SchoolType_Gender");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Subjects", "SchoolType_Gender", c => c.String());
            AddColumn("dbo.Subjects", "SchoolType_Boarding", c => c.Boolean(nullable: false));
            AddColumn("dbo.Subjects", "SchoolType_Category", c => c.String());
            AddColumn("dbo.Exams", "SchoolType_Gender", c => c.String());
            AddColumn("dbo.Exams", "SchoolType_Boarding", c => c.Boolean(nullable: false));
            AddColumn("dbo.Exams", "SchoolType_Category", c => c.String());
            DropColumn("dbo.Subjects", "SchoolId");
            DropColumn("dbo.Exams", "SchoolId");
        }
    }
}
