namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vFieldsMore : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Events", "SchoolId");
            CreateIndex("dbo.ExamScores", "StudentId");
            CreateIndex("dbo.ExamScores", "ExamId");
            CreateIndex("dbo.ExamScores", "SchoolId");
            CreateIndex("dbo.Fees", "PaymentMethodId");
            AddForeignKey("dbo.Events", "SchoolId", "dbo.Schools", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ExamScores", "ExamId", "dbo.Exams", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ExamScores", "SchoolId", "dbo.Schools", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ExamScores", "StudentId", "dbo.Students", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Fees", "PaymentMethodId", "dbo.PaymentMethods", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Fees", "PaymentMethodId", "dbo.PaymentMethods");
            DropForeignKey("dbo.ExamScores", "StudentId", "dbo.Students");
            DropForeignKey("dbo.ExamScores", "SchoolId", "dbo.Schools");
            DropForeignKey("dbo.ExamScores", "ExamId", "dbo.Exams");
            DropForeignKey("dbo.Events", "SchoolId", "dbo.Schools");
            DropIndex("dbo.Fees", new[] { "PaymentMethodId" });
            DropIndex("dbo.ExamScores", new[] { "SchoolId" });
            DropIndex("dbo.ExamScores", new[] { "ExamId" });
            DropIndex("dbo.ExamScores", new[] { "StudentId" });
            DropIndex("dbo.Events", new[] { "SchoolId" });
        }
    }
}
