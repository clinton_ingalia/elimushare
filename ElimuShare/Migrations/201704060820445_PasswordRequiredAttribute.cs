namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PasswordRequiredAttribute : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Parents", "MobileNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "MobileNumber", c => c.String(nullable: false));
            DropColumn("dbo.Parents", "Password");
            DropColumn("dbo.Users", "Password");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Password", c => c.String());
            AddColumn("dbo.Parents", "Password", c => c.String());
            AlterColumn("dbo.Users", "MobileNumber", c => c.String());
            AlterColumn("dbo.Parents", "MobileNumber", c => c.String());
        }
    }
}
