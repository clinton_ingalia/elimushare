namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accessCode : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccessCodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ParentId = c.Guid(nullable: false),
                        Code = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AccessCodes");
        }
    }
}
