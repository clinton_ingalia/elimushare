namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAdmissionNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "AdmissionNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Students", "AdmissionNumber");
        }
    }
}
