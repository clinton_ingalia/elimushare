namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationModelChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "SchoolId", c => c.Guid(nullable: false));
            AddColumn("dbo.Notifications", "SchoolName", c => c.String());
            DropColumn("dbo.Notifications", "AuthorId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notifications", "AuthorId", c => c.Guid(nullable: false));
            DropColumn("dbo.Notifications", "SchoolName");
            DropColumn("dbo.Notifications", "SchoolId");
        }
    }
}
