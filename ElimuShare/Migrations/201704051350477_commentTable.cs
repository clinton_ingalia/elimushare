namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commentTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserOrParentId = c.Guid(nullable: false),
                        EventOrNotificationId = c.Guid(nullable: false),
                        Description = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Schools", "Bio", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Schools", "Bio");
            DropTable("dbo.Comments");
        }
    }
}
