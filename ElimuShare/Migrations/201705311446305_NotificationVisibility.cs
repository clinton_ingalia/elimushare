namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationVisibility : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "Public", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "Public");
        }
    }
}
