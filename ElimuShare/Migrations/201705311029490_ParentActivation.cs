namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParentActivation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Parents", "Activated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Parents", "Activated");
        }
    }
}
