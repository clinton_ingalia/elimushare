namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PasswordChangedToByte : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Parents", "Password", c => c.Binary(nullable: false));
            AddColumn("dbo.Users", "Password", c => c.Binary(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Password");
            DropColumn("dbo.Parents", "Password");
        }
    }
}
