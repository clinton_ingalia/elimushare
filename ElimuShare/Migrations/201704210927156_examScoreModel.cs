namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class examScoreModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exams", "SchoolType_Category", c => c.String());
            AddColumn("dbo.Exams", "SchoolType_Boarding", c => c.Boolean(nullable: false));
            AddColumn("dbo.Exams", "SchoolType_Gender", c => c.String());
            AddColumn("dbo.Exams", "Name", c => c.String());
            AddColumn("dbo.Exams", "Description", c => c.String());
            AddColumn("dbo.Fees", "Paid", c => c.Double(nullable: false));
            AddColumn("dbo.SubjectScores", "ExamId", c => c.Guid(nullable: false));
            AddColumn("dbo.SubjectScores", "Grade", c => c.String());
            DropColumn("dbo.Exams", "StudentId");
            DropColumn("dbo.Exams", "SchoolId");
            DropColumn("dbo.Exams", "Title");
            DropColumn("dbo.Exams", "Year");
            DropColumn("dbo.Exams", "Term");
            DropColumn("dbo.Exams", "ExamType");
            DropColumn("dbo.Exams", "Position");
            DropColumn("dbo.Exams", "TotalStudents");
            DropColumn("dbo.Exams", "Score");
            DropColumn("dbo.Exams", "TotalScore");
            DropColumn("dbo.Exams", "TeacherRemarks");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Exams", "TeacherRemarks", c => c.String());
            AddColumn("dbo.Exams", "TotalScore", c => c.Int(nullable: false));
            AddColumn("dbo.Exams", "Score", c => c.Int(nullable: false));
            AddColumn("dbo.Exams", "TotalStudents", c => c.Int(nullable: false));
            AddColumn("dbo.Exams", "Position", c => c.Int(nullable: false));
            AddColumn("dbo.Exams", "ExamType", c => c.String());
            AddColumn("dbo.Exams", "Term", c => c.Int(nullable: false));
            AddColumn("dbo.Exams", "Year", c => c.DateTime(nullable: false));
            AddColumn("dbo.Exams", "Title", c => c.String());
            AddColumn("dbo.Exams", "SchoolId", c => c.Guid(nullable: false));
            AddColumn("dbo.Exams", "StudentId", c => c.Guid(nullable: false));
            DropColumn("dbo.SubjectScores", "Grade");
            DropColumn("dbo.SubjectScores", "ExamId");
            DropColumn("dbo.Fees", "Paid");
            DropColumn("dbo.Exams", "Description");
            DropColumn("dbo.Exams", "Name");
            DropColumn("dbo.Exams", "SchoolType_Gender");
            DropColumn("dbo.Exams", "SchoolType_Boarding");
            DropColumn("dbo.Exams", "SchoolType_Category");
        }
    }
}
