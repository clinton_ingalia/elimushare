namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class checkifuserorparentexist : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schools", "Mission", c => c.String());
            AddColumn("dbo.Schools", "Vission", c => c.String());
            AddColumn("dbo.Schools", "Motto", c => c.String());
            AddColumn("dbo.Schools", "Phone", c => c.String());
            AddColumn("dbo.Schools", "Email", c => c.String());
            AddColumn("dbo.Schools", "AddressLine", c => c.String());
            AddColumn("dbo.Schools", "PostalCode", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Schools", "PostalCode");
            DropColumn("dbo.Schools", "AddressLine");
            DropColumn("dbo.Schools", "Email");
            DropColumn("dbo.Schools", "Phone");
            DropColumn("dbo.Schools", "Motto");
            DropColumn("dbo.Schools", "Vission");
            DropColumn("dbo.Schools", "Mission");
        }
    }
}
