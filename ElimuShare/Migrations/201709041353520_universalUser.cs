namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class universalUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "UserRoleId", c => c.Guid(nullable: false));
            DropColumn("dbo.Users", "UserTypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "UserTypeId", c => c.Guid(nullable: false));
            DropColumn("dbo.Users", "UserRoleId");
        }
    }
}
