// <auto-generated />
namespace ElimuShare.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class universalUser : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(universalUser));
        
        string IMigrationMetadata.Id
        {
            get { return "201709041353520_universalUser"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
