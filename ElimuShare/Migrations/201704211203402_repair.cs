namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class repair : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExamScores",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        ExamId = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        Title = c.String(),
                        Year = c.DateTime(nullable: false),
                        Term = c.Int(nullable: false),
                        Position = c.Int(nullable: false),
                        TotalStudents = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                        TotalScore = c.Int(nullable: false),
                        Grade = c.String(),
                        TeacherRemarks = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ExamScores");
        }
    }
}
