namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subjectAndSubjectScoreModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exams", "TeacherRemarks", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Exams", "TeacherRemarks");
        }
    }
}
