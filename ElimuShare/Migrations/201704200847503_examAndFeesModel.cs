namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class examAndFeesModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Exams",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        Title = c.String(),
                        Year = c.DateTime(nullable: false),
                        Term = c.Int(nullable: false),
                        ExamType = c.String(),
                        Position = c.Int(nullable: false),
                        TotalStudents = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                        TotalScore = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Fees", "PaymentMethodId", c => c.Guid(nullable: false));
            AddColumn("dbo.Fees", "Year", c => c.DateTime(nullable: false));
            AddColumn("dbo.Fees", "Term", c => c.Int(nullable: false));
            AddColumn("dbo.Fees", "Balance", c => c.Double(nullable: false));
            AddColumn("dbo.Schools", "AdministratorId", c => c.Guid(nullable: false));
            DropColumn("dbo.Fees", "BankId");
            DropColumn("dbo.Fees", "ConfirmationCode");
            DropTable("dbo.Results");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Results",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        SchoolId = c.Guid(nullable: false),
                        Title = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Fees", "ConfirmationCode", c => c.String());
            AddColumn("dbo.Fees", "BankId", c => c.Guid(nullable: false));
            DropColumn("dbo.Schools", "AdministratorId");
            DropColumn("dbo.Fees", "Balance");
            DropColumn("dbo.Fees", "Term");
            DropColumn("dbo.Fees", "Year");
            DropColumn("dbo.Fees", "PaymentMethodId");
            DropTable("dbo.Exams");
        }
    }
}
