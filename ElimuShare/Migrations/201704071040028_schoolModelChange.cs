namespace ElimuShare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class schoolModelChange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Schools", "SchoolType_ID", "dbo.SchoolTypes");
            DropIndex("dbo.Schools", new[] { "SchoolType_ID" });
            AddColumn("dbo.Schools", "SchoolType_Category", c => c.String());
            AddColumn("dbo.Schools", "SchoolType_Boarding", c => c.Boolean(nullable: false));
            AddColumn("dbo.Schools", "SchoolType_Gender", c => c.String());
            DropColumn("dbo.Schools", "SchoolType_ID");
            DropTable("dbo.SchoolTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SchoolTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Category = c.String(),
                        Boarding = c.Boolean(nullable: false),
                        Gender = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Schools", "SchoolType_ID", c => c.Guid());
            DropColumn("dbo.Schools", "SchoolType_Gender");
            DropColumn("dbo.Schools", "SchoolType_Boarding");
            DropColumn("dbo.Schools", "SchoolType_Category");
            CreateIndex("dbo.Schools", "SchoolType_ID");
            AddForeignKey("dbo.Schools", "SchoolType_ID", "dbo.SchoolTypes", "ID");
        }
    }
}
