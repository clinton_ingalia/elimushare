﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ElimuShare.Data.Portal;
using Microsoft.AspNet.SignalR;

namespace ElimuShare.Helpers
{
    public class ElimuShareHub : Hub
    {
        public void SendDashboardMetrics(StatsRow statsRow)
        {
            Clients.All.broadcastDashboardGeneralSummary(statsRow);
        }

        public void Heartbeat()
        {
            Clients.All.heartbeat();
        }

        public override Task OnConnected()
        {
            return (base.OnConnected());
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return (base.OnDisconnected(stopCalled));
        }

        public override Task OnReconnected()
        {
            return (base.OnReconnected());
        }

        public string GetServerTime()
        {
            return DateTime.UtcNow.ToString();
        }
    }
}