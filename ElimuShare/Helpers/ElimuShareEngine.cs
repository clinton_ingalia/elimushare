﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ElimuShare.Data.Model;
using ElimuShare.Data.Portal;
using ElimuShare.Models;
using Microsoft.AspNet.SignalR;

namespace ElimuShare.Helpers
{
    public class ElimuShareEngine
    {
        private IHubContext _hubs;
        private readonly int _pollIntervalMillis;
        private ElimuShareContext db;

        public ElimuShareEngine(int pollIntervalMillis)
        {
            _hubs = GlobalHost.ConnectionManager.GetHubContext<ElimuShareHub>();
            _pollIntervalMillis = pollIntervalMillis;
            db = new ElimuShareContext();
        }

        public async Task OnDashboardGeneralSummaryMonitor()
        {
            while (true)
            {
                await Task.Delay(_pollIntervalMillis);
                StatsRow statsRow = new StatsRow();
                try
                {
                    statsRow = new StatsRow()
                    {
                        Parents = 0
                };
                }
                catch (InvalidOperationException ex)
                {
                    Trace.TraceError(ex.Message);
                    Trace.TraceError(ex.StackTrace);
                }

                _hubs.Clients.All.broadcastDashboardGeneralSummary(statsRow);
                _hubs.Clients.All.serverTime(DateTime.UtcNow.ToString());
            }
        }

        public void Stop(bool immediate)
        {

            //HostingEnvironment.UnregisterObject(this);
        }

        public int GetParentsBySchoolId(Guid SchoolId)
        {
            var students = db.Students.Where(x => x.SchoolId == SchoolId).ToList();
            var parents = db.Parents.ToList();
            var parentsList = new List<Parent>();
            foreach (var student in students)
            {
                foreach (var parent in parents.ToList())
                {
                    if (student.ParentId == parent.ID)
                    {
                        parentsList.Add(parent);
                    }
                }
            }
            var listWithNoDuplicates = parentsList.Distinct().ToList();
            return listWithNoDuplicates.Count;
        }
    }
}