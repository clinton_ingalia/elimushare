﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using ElimuShare.Controllers;
using ElimuShare.Data.ViewModel;
using ElimuShare.Models;
using Microsoft.Owin.Security.OAuth;

namespace ElimuShare.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            

            using (var _authenticationRepository = new AuthenticationRepository())
            {
                var login = new Login()
                {
                    MobileNumber = context.UserName,
                    Password = context.Password
                };

                var responseMessage = _authenticationRepository.UniversalLogin(login);

                if (responseMessage.StatusCode == HttpStatusCode.NotFound)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("role", "user"));

            context.Validated(identity);

        }
    }
}