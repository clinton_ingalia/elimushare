﻿using ElimuShare.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace ElimuShare.Models
{
    public class ElimuShareContext : DbContext
    {
        public ElimuShareContext(): base("name=ElimuShareContext")
        {

        }

        public DbSet<Event> Events { get; set; }
        public DbSet<Fee> Fees { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<SubjectScore> SubjectScores { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Vacancy> Vacancies { get; set; }

        public System.Data.Entity.DbSet<ElimuShare.Data.Model.Shared.UserType> UserTypes { get; set; }

        public System.Data.Entity.DbSet<ElimuShare.Data.Model.Comment> Comments { get; set; }

        public System.Data.Entity.DbSet<ElimuShare.Data.Model.AccessCode> AccessCodes { get; set; }

        public System.Data.Entity.DbSet<ElimuShare.Data.Model.ExamScore> ExamScores { get; set; }

        public System.Data.Entity.DbSet<ElimuShare.Data.Model.VacancyResponse> VacancyResponses { get; set; }

        public System.Data.Entity.DbSet<ElimuShare.Data.Model.UserAccessRole> UserAccessRoles { get; set; }
    }
}