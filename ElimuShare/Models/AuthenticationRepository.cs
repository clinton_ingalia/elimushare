﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ElimuShare.Controllers;
using ElimuShare.Data.ViewModel;
using ElimuShare.Services.HashService;

namespace ElimuShare.Models
{
    /// <summary>
    /// Authentication repository
    /// </summary>
    interface IAuthenticationRepository
    {
        /// <summary>
        /// Logins the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="mobileNumber">Name of the user.</param>
        /// <param name="passsword">The passsword.</param>
        /// <returns></returns>
        HttpResponseMessage Login(int type, string mobileNumber, string passsword);

        string GetUser(string mobileNumber, string passsword);
        HttpResponseMessage UniversalLogin(Login login);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    /// <seealso cref="ElimuShare.Models.IAuthenticationRepository" />
    public class AuthenticationRepository: ApiController , IAuthenticationRepository
    {
        /// <summary>
        /// The database
        /// </summary>
        private ElimuShareContext db = new ElimuShareContext();
        /// <summary>
        /// The hash service
        /// </summary>
        private HashService _hashService = new HashService();


        public HttpResponseMessage UniversalLogin(Login login)
        {
            var myApiController = new UsersController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };
            var user = db.Users.FirstOrDefault(x => x.MobileNumber == login.MobileNumber);
            if (user == null)
                return myApiController.Request.CreateResponse(HttpStatusCode.NotFound, "No user found with that Mobile Number");
            bool verifyPassword = _hashService.Verify(user.ID.ToString().Replace(@"-", ""), user.Password, login.Password);
            return verifyPassword ? myApiController.Request.CreateResponse(HttpStatusCode.OK, user) : myApiController.Request.CreateResponse(HttpStatusCode.NotFound, "Password do not match");
        }



        /// <summary>
        /// Logins the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="mobileNumber">The mobile number.</param>
        /// <param name="passsword">The passsword.</param>
        /// <returns></returns>
        public HttpResponseMessage Login(int type, string mobileNumber, string passsword)
        {

            if (type == 0)
            {
                var user = db.Users.Where(u => u.MobileNumber == mobileNumber)
                    .FirstOrDefault();
                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No user found with that Mobile Number");
                bool verifyPassword = _hashService.Verify(user.ID.ToString().Replace(@"-", ""), user.Password, passsword);
                return verifyPassword ? Request.CreateResponse(HttpStatusCode.OK, user) : Request.CreateResponse(HttpStatusCode.NotFound, "Password do not match");
            }
            else
            {
                var user = db.Parents.Where(u => u.MobileNumber == mobileNumber)
                    .FirstOrDefault();
                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No user found with that Mobile Number");
                bool verifyPassword = _hashService.Verify(user.ID.ToString().Replace(@"-", ""), user.Password, passsword);
                return verifyPassword ? Request.CreateResponse(HttpStatusCode.OK, user) : Request.CreateResponse(HttpStatusCode.NotFound, "Password do not match");
            }
        }

        public string GetUser(string mobileNumber, string passsword)
        {
            var parentDetails = db.Parents.Where(u => u.MobileNumber == mobileNumber)
                .FirstOrDefault();
            var userDetails = db.Users.Where(u => u.MobileNumber == mobileNumber)
                .FirstOrDefault();

            if (parentDetails == null && userDetails == null) return null;
            var parentVerified = "";
            var userVerified = "";
            if (parentDetails != null)
            {
                var verifyPasswordParent = _hashService.Verify(parentDetails.ID.ToString().Replace(@"-", ""), parentDetails.Password, passsword);
                parentVerified = "1";
                return parentVerified;
            }
            if (userDetails == null) return null;
            var verifyPasswordUser = _hashService.Verify(userDetails.ID.ToString().Replace(@"-", ""), userDetails.Password, passsword);
            userVerified = "1";
            return userVerified;

        }
    }
}