﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ElimuShare.Data.Model;

namespace ElimuShare.Models
{
    public class ParentRepository
    {
        private ElimuShareContext db = new ElimuShareContext();
        public bool IsRegistered(string phone)
        {
            var user = db.Parents.Where(p => p.MobileNumber == phone).FirstOrDefault();
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}