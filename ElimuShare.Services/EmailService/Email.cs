﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ElimuShare.Services.EmailService
{
    public class Email
    {
        public void sendEmail(string email, string message)
        {
            MailMessage mail = new MailMessage("cloud@elimushare.com", email);
            NetworkCredential basicCredential =
                new NetworkCredential("cloud@elimushare.com", "3l1mush@r3");
            SmtpClient client = new SmtpClient();
            client.Port = 465;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = basicCredential;
            client.UseDefaultCredentials = false;
            client.Host = "elimushare.com";
            mail.Subject = "School Registration Successful";
            mail.Body = message;
            try
            {
                client.Send(mail);
            }
            catch (Exception e)
            {
                 
            Console.WriteLine(e);
               // throw;
            }
            
        }
    }
}
