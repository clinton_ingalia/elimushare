﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace ElimuShare.Services.HashService
{
    public class HashService
    {
        public string Hash { get; private set; }
        public string Salt { get; private set; }

        public string SaltedHash(string password)
        {
            var saltBytes = new byte[32];
            using (var provider = new RNGCryptoServiceProvider())
                provider.GetNonZeroBytes(saltBytes);
            Salt = Convert.ToBase64String(saltBytes);
            Hash = ComputeHash(Salt, password);
            return Hash;
        }

        public string ComputeHash(string salt, string password)
        {
            var saltBytes = Convert.FromBase64String(salt);
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, saltBytes, 1000))
                return Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256));
        }

        public byte[] ComputeHashByte(string salt, string password)
        {
            var saltBytes = Convert.FromBase64String(salt);
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, saltBytes, 1000))
                return rfc2898DeriveBytes.GetBytes(256);
        }

        public bool Verify(string salt, byte[] hash, string password)
        {
            HashService hs = new HashService();
            byte[] confirmHash = hs.ComputeHashByte(salt, password);
            if (hash.SequenceEqual(confirmHash))
            {
                return true;
            }
            return false;
        }
    }
}